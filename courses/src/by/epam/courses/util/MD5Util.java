package by.epam.courses.util;

import by.epam.courses.exception.TechnicalException;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This class encapsulates method responsible for MD5 encryption
 *         Created on April 15, 2015.
 */
public class MD5Util {
    /**
     * This method implements MD5 encryption, which is used for password encryption in the application.
     * @param st  the {@code String} representation of the entered password
     * @return  the {@code String} representation of the MD5-encrypted password which can be safely put to the
     * database.
     * @throws TechnicalException
     */
    public String md5Custom(String st) throws TechnicalException {
	MessageDigest messageDigest;
	byte[] digest;
	try {
	    messageDigest = MessageDigest.getInstance("MD5");
	    messageDigest.reset();
	    messageDigest.update(st.getBytes());
	    digest = messageDigest.digest();
	} catch (NoSuchAlgorithmException e) {
	    throw new TechnicalException("Algorithm MD5 is not supported. " + e);
	}
	BigInteger bigInt = new BigInteger(1, digest);
	String md5Hex = bigInt.toString(16);
	while( md5Hex.length() < 32 ){
	    md5Hex = "0" + md5Hex;
	}
	return md5Hex;
    }
}
