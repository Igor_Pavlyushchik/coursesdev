package by.epam.courses.servlet;

import by.epam.courses.comand.ActionCommand;
import by.epam.courses.comand.factory.ActionFactory;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.pool.ConnectionPool;
import by.epam.courses.resource.ConfigurationManager;
import by.epam.courses.resource.MessageManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Properties;

/**
 * The only servlet of the application, which implements controller function, receiving requests from the
 * client, calling appropriate {@code ActionCommand} implementation and redirecting the request to the
 * relevant answer page.
 * @author Igor Pavlyushchik
 *
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {
    private static final long serialVersionUID = -5563436655417677165L;

    static Logger logger;
    private static ConnectionPool connectionPool = null;

    /**
     * Initializes the logger and the connectionPool field.
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
	super.init();
	final String LOG4J = "/resources/log4j.properties";
	Properties logProperties = new Properties();
	try {
	    logProperties.load(Controller.class.getResourceAsStream(LOG4J));
	    PropertyConfigurator.configure(logProperties);
	} catch (IOException e) {
	    throw new ExceptionInInitializerError("Error trying to initialize logger." + e);
	}
	logger = Logger.getLogger(Controller.class);

	connectionPool = ConnectionPool.getConnectionPool();
    }

    /**
     * Called by the servlet container to indicate to a servlet that the servlet is being taken out of service.
     * Releases connection pool by calling {@Link ConnectionPool#releasePool} method.
     *
     */
    @Override
    public void destroy() {
	    super.destroy();
	    connectionPool.releasePool();
	    logger.info("Connection pool is released.");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
	processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
	processRequest(request, response);
    }

    /**
     * This method encapsulates main functionality of the servlet
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException{
	String page = null;
	// determine the command that came from JSP
	ActionFactory client = new ActionFactory();
	ActionCommand command = client.defineCommand(request);

    /*
    * calling implemented execute() method and transferring the request to the corresponding class
    */
	try {
	    page = command.execute(request);
	} catch (TechnicalException e) {
	    logger.error("Unable to obtain page calling execute() method. ", e);
	}
	// the method returns the response page
	if (page != null) {
	    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
	// calling the response page for the request
	    dispatcher.forward(request, response);
	} else {
	// setting page with error message
	    page = ConfigurationManager.getProperty("path.page.index");
	    request.getSession().setAttribute("nullPage",
		new MessageManager(request).getProperty("message.null_page"));
	    response.sendRedirect(request.getContextPath() + page);
	}
    }
}
