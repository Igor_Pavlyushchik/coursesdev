package by.epam.courses.dao;

import by.epam.courses.exception.DAOException;
import by.epam.courses.subject.Course;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * This class encapsulates methods working mainly with the {@code course} table of the database.
 * @author Igor Pavlyushchik
 *         Created on April 26, 2015.
 */
public class CourseDAO extends AbstractDAO<Integer, Course> {
    private static final String SQL_SELECT_ALL_COURSES = "SELECT * FROM course";
    private static final String SQL_SELECT_TITLE_BY_LOGIN = "SELECT title FROM course INNER JOIN professor " +
        "ON professor_id = professor_id_fk WHERE login=?";
    private static final String SQL_SELECT_COURSES_BY_LOGIN = "SELECT course_id, title, professor_id_fk FROM course" +
        " INNER JOIN professor ON professor_id = professor_id_fk WHERE login=?";

    public CourseDAO() throws DAOException {
    }

    public String findTitleByLogin(String login) throws DAOException {
        String title = null;
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_TITLE_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    title = resultSet.getString("title");
                }
            } catch (SQLException e) {
            throw new DAOException("Unable to find title of the course by professor login. " + e);
        }
        return title;
    }

    public List<Course> findCoursesByLogin (String login) throws DAOException {
        List<Course> courses = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_COURSES_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Course course = new Course();
                course.setCourseId(resultSet.getInt("course_id"));
                course.setTitle(resultSet.getString("title"));
                course.setProfessorId(resultSet.getInt("professor_id_fk"));
                courses.add(course);
            }
        }   catch (SQLException e) {
            throw new DAOException("SQL exception for list of courses failed. " + e);
        }
        return courses;
    }

    @Override
    public List<Course> findAll() throws DAOException {
	List<Course> courses = new ArrayList<>();
	try(Statement statement = connection.createStatement()) {
	    ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_COURSES);
            while (resultSet.next()) {
                Course course = new Course();
                course.setCourseId(resultSet.getInt("course_id"));
                course.setTitle(resultSet.getString("title"));
                course.setProfessorId(resultSet.getInt("professor_id_fk"));
                courses.add(course);
            }
	}   catch (SQLException e) {
	    throw new DAOException("SQL exception (request or table failed): " + e);
	}
	return courses;
    }

    @Override
    public Course findEntityById(Integer id) {
	return null;
    }

    @Override
    public boolean delete(Integer id) {
	return false;
    }

    @Override
    public boolean delete(Course entity) {
	return false;
    }

    @Override
    public boolean create(Course entity) {
	return false;
    }

    @Override
    public int update(Course entity) throws DAOException {
	return 0;
    }
}
