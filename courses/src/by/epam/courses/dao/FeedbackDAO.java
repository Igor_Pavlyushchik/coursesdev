package by.epam.courses.dao;

import by.epam.courses.exception.DAOException;
import by.epam.courses.subject.Feedback;
import by.epam.courses.subject.Login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * This method implements functionality of dealing mainly with the {@code feedback} table of the database.
 * @author Igor Pavlyushchik
 *         Created on April 28, 2015.
 */
public class FeedbackDAO extends AbstractDAO<Integer, Feedback> {
    private static final String SIGN_UP = "INSERT INTO feedback (course_id_fk, student_id_fk, status)" +
	" VALUES (?, ?, 'not started')";
    private static final String SQL_CANCEL = "DELETE FROM feedback WHERE feedback_id=?";
    private static final String SQL_FEEDBACK_STUDENT_PROFESSOR = "SELECT feedback_id, course_id_fk, status, feedback, grade FROM feedback" +
        " INNER JOIN student ON student_id = student_id_fk INNER JOIN course ON course_id = course_id_fk" +
        " INNER JOIN professor ON professor_id = professor_id_fk WHERE student_id=? AND professor.login=?";
    private static final String SQL_FEEDBACK_STUDENT_COURSE = "SELECT feedback_id, course_id_fk, status, feedback," +
        " grade FROM feedback WHERE student_id_fk = ? AND course_id_fk = ?";
    private static final String SQL_FEEDBACK_UPDATE = "UPDATE feedback SET status=?, grade=?, feedback=?" +
        " WHERE feedback_id=?";
    private static final String SQL_COUNT_FEEDBACK_BY_STUDENT_ID = "SELECT COUNT(feedback_id) FROM feedback" +
        " WHERE student_id_fk = ?";

    public FeedbackDAO() throws DAOException {
    }

    public int countFeedbackByStudentId (int studentId) throws DAOException {
        int feedbackCount = 0;
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_COUNT_FEEDBACK_BY_STUDENT_ID)) {
            preparedStatement.setInt(1, studentId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                feedbackCount = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL exception, unable to count feedbackId for the student: ", e);
        }
        return feedbackCount;
    }

    public Feedback findFeedbackByStudentIdCourseId (int studentId, int courseId) throws DAOException {
        Feedback feedback = new Feedback();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FEEDBACK_STUDENT_COURSE)) {
            preparedStatement.setInt(1, studentId);
            preparedStatement.setInt(2, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                feedback.setFeedbackId(resultSet.getInt("feedback_id"));
                feedback.setCourseId(resultSet.getInt("course_id_fk"));
                feedback.setStatus(resultSet.getString("status"));
                feedback.setFeedback(resultSet.getString("feedback"));
                feedback.setGrade(resultSet.getInt("grade"));
            }
        }  catch (SQLException e) {
            throw new DAOException("SQL exception, unable to obtain feedback by studentId and courseId: " + e);
        }
        return feedback;
    }

    public Feedback findFeedbackStudentIdProfessorLogin(int studentId, String login) throws DAOException {
        Feedback feedback = new Feedback();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FEEDBACK_STUDENT_PROFESSOR)) {
            preparedStatement.setInt(1, studentId);
            preparedStatement.setString(2, login);
            ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    feedback.setFeedbackId(resultSet.getInt("feedback_id"));
                    feedback.setCourseId(resultSet.getInt("course_id_fk"));
                    feedback.setStatus(resultSet.getString("status"));
                    feedback.setFeedback(resultSet.getString("feedback"));
                    feedback.setGrade(resultSet.getInt("grade"));
                }
        }  catch (SQLException e) {
            throw new DAOException("SQL exception, unable to obtain feedback by studentId and professor login: " + e);
        }
        return feedback;
    }

    @Override
    public List<Feedback> findAll() throws DAOException {
	return null;
    }

    @Override
    public Feedback findEntityById(Integer id) {
	return null;
    }

    @Override
    public boolean delete(Integer id) throws DAOException {
	try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_CANCEL)) {
	    preparedStatement.setInt(1, id);
	    preparedStatement.executeUpdate();
	}  catch (SQLException e) {
	    throw new DAOException("SQL exception trying to cancel signing-up for the course: " + e);
	}
	return true;
    }

    @Override
    public boolean delete(Feedback entity) {
	return false;
    }

    @Override
    public boolean create(Feedback entity) throws DAOException {
	try(PreparedStatement preparedStatement = connection.prepareStatement(SIGN_UP)) {
	    preparedStatement.setInt(1, entity.getCourseId());
	    preparedStatement.setInt(2, entity.getStudentId());
	    preparedStatement.executeUpdate();
	}  catch (SQLException e) {
	    throw new DAOException("SQL exception trying to sign-up for the course: " + e);
	}
	return true;
    }

    @Override
    public int update(Feedback entity) throws DAOException {
        int rowsAffected;
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FEEDBACK_UPDATE)) {
            preparedStatement.setString(1, entity.getStatus());
            preparedStatement.setInt(2, entity.getGrade());
            preparedStatement.setString(3, entity.getFeedback());
            preparedStatement.setInt(4, entity.getFeedbackId());
            rowsAffected = preparedStatement.executeUpdate();
        }  catch (SQLException e) {
            throw new DAOException("Unable to upgrade feedback. " + e);
        }
        return rowsAffected;
    }
}
