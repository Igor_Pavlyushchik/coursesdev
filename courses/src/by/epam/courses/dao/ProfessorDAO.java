package by.epam.courses.dao;

import by.epam.courses.exception.DAOException;
import by.epam.courses.subject.Login;
import by.epam.courses.subject.Professor;
import by.epam.courses.subject.Student;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This method implements functionality of dealing mainly with the {@code professor} table of the database.
 * @author Igor Pavlyushchik
 *         Created on April 27, 2015.
 */
public class ProfessorDAO extends AbstractDAO<Integer, Professor> {
    private static final String SQL_SELECT_PASSWORD = "SELECT professor_id, first_name, second_name, phone, professor.login," +
	" login.login, password, role FROM professor INNER JOIN login ON professor.login = login.login WHERE professor.login=?";

    public ProfessorDAO() throws DAOException {
	super();
    }

    public Map<Login, Professor> findProfessor(String login) throws DAOException {
	Map<Login, Professor> loginProfessorMap = new HashMap<>();
	try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_PASSWORD)) {
	    preparedStatement.setString(1, login);
	    ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
		    Professor professor = new Professor();
		    Login loginForMap = new Login();
		    professor.setProfessorId(resultSet.getInt("professor_id"));
		    professor.setFirstName(resultSet.getString("first_name"));
		    professor.setSecondName(resultSet.getString("second_name"));
		    professor.setPhone(resultSet.getInt("phone"));
		    professor.setLogin(resultSet.getString("professor.login"));
		    loginForMap.setLogin(resultSet.getString("login.login"));
		    loginForMap.setPassword(resultSet.getString("password"));
		    loginForMap.setRole(resultSet.getString("role"));
		    loginProfessorMap.put(loginForMap, professor);
		}
	}  catch (SQLException e) {
	    throw new DAOException("SQL exception (request or table failed): " + e);
	}
	return loginProfessorMap;
    }

    @Override
    public List<Professor> findAll() throws DAOException {
	return null;
    }

    @Override
    public Professor findEntityById(Integer id) {
	return null;
    }

    @Override
    public boolean delete(Integer id) {
	return false;
    }

    @Override
    public boolean delete(Professor entity) {
	return false;
    }

    @Override
    public boolean create(Professor entity) {
	return false;
    }

    @Override
    public int update(Professor entity) throws DAOException {
	return 0;
    }
}
