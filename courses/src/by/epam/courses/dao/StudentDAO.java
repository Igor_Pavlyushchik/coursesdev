package by.epam.courses.dao;

import by.epam.courses.exception.DAOException;
import by.epam.courses.subject.Course;
import by.epam.courses.subject.Feedback;
import by.epam.courses.subject.Login;
import by.epam.courses.subject.Student;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This method implements functionality of dealing mainly with the {@code student} table of the database.
 * @author Igor Pavlyushchik
 *         Created on April 05, 2015.
 */
public class StudentDAO extends AbstractDAO<Integer, Student> {
    private static final String SQL_SELECT_ALL_STUDENTS = "SELECT * FROM student ORDER BY student_id";
    private static final String SQL_SELECT_PASSWORD = "SELECT student_id, first_name, second_name, phone, student.login," +
	" login.login, password, role FROM student INNER JOIN login ON student.login = login.login WHERE student.login=?";
    private static final String SQL_STUDENT_UPDATE = "UPDATE student SET phone=?, login=? WHERE student_id=?";
    private static final String SQL_SELECT_FEEDBACK_BY_LOGIN = "SELECT feedback_id, status, feedback, grade FROM feedback " +
	"INNER JOIN student ON student_id = student_id_fk INNER JOIN course ON course_id = course_id_fk " +
	"WHERE login=? AND course_id=?";
    private static final String SQL_SELECT_ID_BY_LOGIN = "SELECT student_id FROM student WHERE login=?";
    private static final String SQL_ADD_STUDENT = "INSERT into student (first_name, second_name, " +
	"phone, login) VALUES (?, ?, ?, ?)";
    private static final String SQL_DELETE = "DELETE FROM student WHERE login=?";

    public StudentDAO() throws DAOException {
	super();
    }

    public int findStudentId (String login) throws DAOException {
	int studentId = -1;
	try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ID_BY_LOGIN)) {
	    preparedStatement.setString(1, login);
	    ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
		    studentId = resultSet.getInt("student_id");
		}
	}  catch (SQLException e) {
	    throw new DAOException("SQL exception (request or table failed): " + e);
	}
	return studentId;
    }

    public Feedback findFeedbackByLogin (String login, int courseId) throws DAOException {
	Feedback feedback = new Feedback();
	try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_FEEDBACK_BY_LOGIN)) {
	    preparedStatement.setString(1, login);
	    preparedStatement.setInt(2, courseId);
	    ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
		    feedback.setFeedbackId(resultSet.getInt("feedback_id"));
		    feedback.setStatus(resultSet.getString("status"));
		    feedback.setFeedback(resultSet.getString("feedback"));
		    feedback.setGrade(resultSet.getInt("grade"));
		}
	}  catch (SQLException e) {
	    throw new DAOException("SQL exception (request or table failed): " + e);
	}
	return feedback;
    }

    public Map<Login, Student> findStudent(String login) throws DAOException {
	Map<Login, Student> loginStudentMap = new HashMap<>();
	try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_PASSWORD)) {
	    preparedStatement.setString(1, login);
	    ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
		    Student student = new Student();
		    Login loginForMap = new Login();
		    student.setStudentId(resultSet.getInt("student_id"));
		    student.setFirstName(resultSet.getString("first_name"));
		    student.setSecondName(resultSet.getString("second_name"));
		    student.setPhone(resultSet.getInt("phone"));
		    student.setLogin(resultSet.getString("student.login"));
		    loginForMap.setLogin(resultSet.getString("login.login"));
		    loginForMap.setPassword(resultSet.getString("password"));
		    loginForMap.setRole(resultSet.getString("role"));
		    loginStudentMap.put(loginForMap, student);
		}
	}  catch (SQLException e) {
	    throw new DAOException("SQL exception (request or table failed): " + e);
	}
	return loginStudentMap;
    }

    @Override
    public List<Student> findAll() throws DAOException {
	List<Student> students = new ArrayList<>();
	try(Statement statement = connection.createStatement()) {
	    ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_STUDENTS);
		while (resultSet.next()) {
		    Student student = new Student();
		    student.setStudentId(resultSet.getInt("student_id"));
		    student.setFirstName(resultSet.getString("first_name"));
		    student.setSecondName(resultSet.getString("second_name"));
		    student.setPhone(resultSet.getInt("phone"));
		    student.setLogin(resultSet.getString("login"));
		    students.add(student);
		}
	}   catch (SQLException e) {
	    throw new DAOException("SQL exception (request or table failed): " + e);
	}
	return students;
    }

    @Override
    public Student findEntityById(Integer id) {
	return null;
    }

    @Override
    public boolean delete(Integer id) {
	return false;
    }

    @Override
    public boolean delete(Student entity) throws DAOException {
	try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)) {
	    preparedStatement.setString(1, entity.getLogin());
	    preparedStatement.executeUpdate();
	}  catch (SQLException e) {
	    throw new DAOException("SQL exception trying to cancel signing-up for the course: " + e);
	}
	return true;
    }

    @Override
    public boolean create(Student entity) throws DAOException {
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_STUDENT)) {
	    preparedStatement.setString(1, entity.getFirstName());
	    preparedStatement.setString(2, entity.getSecondName());
	    preparedStatement.setInt(3, entity.getPhone());
	    preparedStatement.setString(4, entity.getLogin());
	    preparedStatement.executeUpdate();
	}  catch (SQLException e) {
	    throw new DAOException("SQL exception while trying to add student. " + e);
	}
	return true;
    }

    @Override
    public int update(Student entity) throws DAOException {
	int rowsAffected;
	try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_STUDENT_UPDATE)) {
	    preparedStatement.setInt(1, entity.getPhone());
	    preparedStatement.setString(2, entity.getLogin());
	    preparedStatement.setInt(3, entity.getStudentId());
	    rowsAffected = preparedStatement.executeUpdate();
	}
	 catch (SQLException e) {
	    throw new DAOException("SQL exception (request or table failed): " + e);
	}
	return rowsAffected;
    }
}
