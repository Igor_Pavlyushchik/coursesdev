package by.epam.courses.dao;

import by.epam.courses.exception.DAOException;
import by.epam.courses.subject.Login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * This method implements functionality of dealing mainly with the {@code login} table of the database.
 * @author Igor Pavlyushchik
 *         Created on April 07, 2015.
 */
public class  LoginDAO extends AbstractDAO<Integer, Login> {
    private static final String SQL_LOGIN_UPDATE = "UPDATE login SET login=?, password=? WHERE login=?";
    private static final String SQL_PASSWORD_UPDATE = "UPDATE login SET password=? WHERE login=?";
    private static final String SQL_ADD_LOGIN = "INSERT INTO login (login, password, role) VALUES" +
	"(?, ?, 'student')";
    private static final String SQL_SELECT_ALL_LOGIN = "SELECT * FROM login";
    private static final String SQL_DELETE = "DELETE FROM login WHERE login=?";

    public LoginDAO() throws DAOException {
	super();
    }

    @Override
    public List<Login> findAll() throws DAOException {
	List<Login> loginList = new ArrayList<>();
	try(Statement statement = connection.createStatement()) {
	    ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_LOGIN);
	    while(resultSet.next()) {
		 Login login = new Login();
		 login.setLogin(resultSet.getString("login"));
		 login.setLogin(resultSet.getString("password"));
		 login.setLogin(resultSet.getString("role"));
		loginList.add(login);
	    }
	} catch (SQLException e) {
	    throw new DAOException("Unable to find all login entries. " + e);
	}
	return loginList;
    }

    @Override
    public Login findEntityById(Integer id) {
	return null;
    }

    @Override
    public boolean delete(Integer id) {
	return false;
    }

    @Override
    public boolean delete(Login entity) throws DAOException {
	try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)) {
	    preparedStatement.setString(1, entity.getLogin());
	    preparedStatement.executeUpdate();
	}  catch (SQLException e) {
	    throw new DAOException("SQL exception trying to delete login entity: " + e);
	}
	return true;
    }

    @Override
    public boolean create(Login entity) throws DAOException {
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_LOGIN)) {
	    preparedStatement.setString(1, entity.getLogin());
	    preparedStatement.setString(2, entity.getPassword());
	    preparedStatement.executeUpdate();
	}  catch (SQLException e) {
	    throw new DAOException("SQL exception, trying to add login. " + e);
	}
	return true;
    }

    @Override
    public int update(Login entity) throws DAOException {
	int rowsAffected;
	try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_PASSWORD_UPDATE)) {
	    preparedStatement.setString(1, entity.getPassword());
	    preparedStatement.setString(2, entity.getLogin());
	    rowsAffected = preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DAOException("SQL exception, trying to update login" + e);
	}
	return rowsAffected;
    }

    public int update(Login entity, String o) throws DAOException {
	int rowsAffected;
	try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOGIN_UPDATE)) {
	    preparedStatement.setString(1, entity.getLogin());
	    preparedStatement.setString(2, entity.getPassword());
	    preparedStatement.setString(3, o);
	    rowsAffected = preparedStatement.executeUpdate();
    	} catch (SQLException e) {
	    throw new DAOException("Unable to update login" + e);
	}
	return rowsAffected;
    }
}
