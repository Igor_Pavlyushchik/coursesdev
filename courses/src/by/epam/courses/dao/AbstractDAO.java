package by.epam.courses.dao;


import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.pool.ConnectionPool;
import by.epam.courses.subject.Entity;

import java.sql.Connection;
import java.util.List;

/**
 * The abstract class which has one constructor, one implemented method and two fields and serves as a general
 * class for all DAO classes of the application.
 * @author Igor Pavlyushchik
 *         Created on April 05, 2015.
 */
public abstract class AbstractDAO <K, T extends Entity> implements AutoCloseable {
    protected static ConnectionPool connectionPool = ConnectionPool.getConnectionPool();
    protected Connection connection;

    /**
     * This constructor instantiates connection variable taking connection from the connection pool.
     * @throws DAOException
     */
    public AbstractDAO() throws DAOException {
        try {
            connection = connectionPool.getConnection();
        } catch (TechnicalException e) {
            throw new DAOException("Unable to get connection from the pool." + e);
        }
    }

    public abstract List<T> findAll() throws DAOException;
    public abstract T findEntityById(K id);
    public abstract boolean delete(K id) throws DAOException;
    public abstract boolean delete(T entity) throws DAOException;
    public abstract boolean create(T entity) throws DAOException;
    public abstract int update(T entity) throws DAOException;

    /**
     * This method overrides close method of the {@code AutoClosable} interface returning not longer  used
     * connection to the pool.
     */
    @Override
    public void close()  {
         connectionPool.closeConnection(connection);
    }
}
