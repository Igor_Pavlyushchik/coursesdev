package by.epam.courses.logic;

import by.epam.courses.dao.ProfessorDAO;
import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.subject.Login;
import by.epam.courses.subject.Professor;
import by.epam.courses.util.MD5Util;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * his class encapsulates method for checking correctness of the professor's login and password.
 * @author Igor Pavlyushchik
 *         Created on March 31, 2015.
 */
public class LoginProfessorLogic {
    /**
     * This method checks if the entered login and password are valid login and password of the professor.
     * In positive scenario the method returns {@code Map<Login, Professor>} map and in the negative -
     * null.
     * @param enterLogin  login, entered by the professor
     * @param enterPass   password, entered by the professor
     * @return   {@code Map<Login, Professor>} which consists of the {@code Login} and {@code Professor} entries
     * which correspond to the logged in professor
     * @throws TechnicalException
     */
    public Map<Login, Professor> checkLogin (String enterLogin, String enterPass) throws TechnicalException {
        Map<Login, Professor> loginProfessorMap;
        String password;
        try (ProfessorDAO professorDAO = new ProfessorDAO()){
            loginProfessorMap = professorDAO.findProfessor(enterLogin);
        } catch (DAOException e) {
            throw new TechnicalException("Unable to get login - professor map." + e);
        }
        Set<Map.Entry<Login, Professor>> entries = loginProfessorMap.entrySet();
        Iterator<Map.Entry<Login, Professor>> iterator = entries.iterator();
        if(iterator.hasNext()) {
            Map.Entry<Login, Professor> loginProfessorEntry = iterator.next();
            password = String.valueOf(loginProfessorEntry.getKey().getPassword());
            enterPass = new MD5Util().md5Custom(enterPass);
            if(!password.equals(enterPass)) {
                loginProfessorMap = null;
            }
        } else {
            loginProfessorMap = null;
        }
        return loginProfessorMap;
    }
}
