package by.epam.courses.logic;

import by.epam.courses.dao.CourseDAO;
import by.epam.courses.dao.FeedbackDAO;
import by.epam.courses.dao.StudentDAO;
import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.resource.ConfigurationManager;
import by.epam.courses.subject.Course;
import by.epam.courses.subject.Feedback;
import by.epam.courses.subject.Student;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This class encapsulates method, responsible for extracting information from the database necessary for
 * displaying feedback table of the all students and courses of the given professor. Also created page gives
 * access to feedback editing and deletion of students, who has not registered for any course of the any professor.
 * @author Igor Pavlyushchik
 *         Created on May 04, 2015.
 */
public class ManageStudentsLogic {
    private final static String LOGIN = "login";

    /**
     * This method prepares {@code String} representation of the path to the jsp with the feedback table of all the
     * students and courses of the current professor.
     * @param request  request of the professor
     * @return   {@code String} value of the path to the student's feedback jsp
     * @throws TechnicalException
     */
    public String manageStudents(HttpServletRequest request) throws TechnicalException {
	Map<Student, Map<Course, Feedback>> map = new HashMap<>();
	Map<Student, Boolean> studentBooleanMap = new HashMap<>();
	try(StudentDAO studentDAO = new StudentDAO(); FeedbackDAO feedbackDAO = new FeedbackDAO();
		CourseDAO courseDAO = new CourseDAO()) {
	    List<Student> studentList = studentDAO.findAll();
	    Collections.sort(studentList, new Comparator<Student>() {
		@Override
		public int compare(Student o1, Student o2) {
		    int result;
		    result = o1.getSecondName().compareTo(o2.getSecondName());
		    if (result != 0) {
			return result;
		    } else {
			result = o1.getFirstName().compareTo(o2.getFirstName());
		    }
		    if (result != 0) {
			return result;
		    } else {
			result = o1.getLogin().compareTo(o2.getLogin());
		    }
		    return result;
		}
	    });
	    request.setAttribute("studentList", studentList);
	    for( Student student : studentList) {
		if(feedbackDAO.countFeedbackByStudentId(student.getStudentId()) > 0) {
		    studentBooleanMap.put(student, false);
		}  else {
		    studentBooleanMap.put(student, true);
		}
	    }
	    request.setAttribute("studentBooleanMap", studentBooleanMap);

	    String login = (String)request.getSession().getAttribute(LOGIN);
	    List<Course> courseList = courseDAO.findCoursesByLogin(login);
	    request.setAttribute("courseList", courseList);
	    Iterator<Student> studentIterator = studentList.iterator();
	    while (studentIterator.hasNext()) {
		Student student = studentIterator.next();
		Iterator<Course> courseIterator = courseList.iterator();
		Map<Course, Feedback> courseFeedbackMap = new HashMap<>();
		while (courseIterator.hasNext()) {
		    Course course = courseIterator.next();
		    Feedback feedback = feedbackDAO.findFeedbackByStudentIdCourseId(student.getStudentId(),
												course.getCourseId());
		    courseFeedbackMap.put(course, feedback);
		}
		map.put(student, courseFeedbackMap);
	    }
	    request.setAttribute("map", map);
	} catch (DAOException e) {
	    throw new TechnicalException("Unable to extract student and feedback information from the database. " + e);
	}
	return ConfigurationManager.getProperty("path.page.students_feedback");
    }
}
