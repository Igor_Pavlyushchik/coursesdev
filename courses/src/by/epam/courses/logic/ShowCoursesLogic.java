package by.epam.courses.logic;

import by.epam.courses.dao.CourseDAO;
import by.epam.courses.dao.StudentDAO;
import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.resource.ConfigurationManager;
import by.epam.courses.subject.Course;
import by.epam.courses.subject.Feedback;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class encapsulates method responsible for the preparation of the {@code String} value of the path
 * to the courses jsp, which displays all courses the student has taken and can take and provides access to the
 * functionality of signing-up and cancelling signing-up for the course.
 * @author Igor Pavlyushchik
 *         Created on April 28, 2015.
 */
public class ShowCoursesLogic {
    private static final String LOGIN = "login";

    /**
     * This method responsible for the preparation of the {@code String} value of the path
     * to the courses jsp, which displays all courses the student has taken and can take,
     * feedback, current status and grade  and provides access to the
     * functionality of signing-up and cancelling signing-up for the course.
     * @param request  request from the student
     * @return  {@code String} value of the path to the student's courses jsp
     * @throws TechnicalException
     */
    public String prepareCoursesPage (HttpServletRequest request) throws TechnicalException {
	String page;
	Map<Course, Feedback> map = new HashMap<>();
	try(CourseDAO courseDAO = new CourseDAO(); StudentDAO studentDAO = new StudentDAO()) {
	    List<Course> courses = courseDAO.findAll();
	    Feedback feedback;
	    String login = (String)request.getSession().getAttribute(LOGIN);
	    for(Course course : courses) {
		feedback = studentDAO.findFeedbackByLogin(login, course.getCourseId());
		map.put(course, feedback);
	    }
	    Set<Map.Entry<Course, Feedback>> entries = map.entrySet();
	    List<Map.Entry<Course, Feedback>> list = new LinkedList<>(entries);
	    Collections.sort(list, new Comparator<Map.Entry<Course, Feedback>>() {
		@Override
		public int compare(Map.Entry<Course, Feedback> o1, Map.Entry<Course, Feedback> o2) {
		    return o1.getKey().getCourseId() - o2.getKey().getCourseId();
		}
	    });
	    request.setAttribute("feedback_list", list);
	} catch (DAOException e) {
	    throw new TechnicalException("Unable to get courses and feedback for given student login. " + e);
	}
	page = ConfigurationManager.getProperty("path.page.student_courses");
	return page;
    }
}
