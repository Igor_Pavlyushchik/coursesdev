package by.epam.courses.logic;

import by.epam.courses.dao.StudentDAO;
import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.subject.Login;
import by.epam.courses.subject.Student;
import by.epam.courses.util.MD5Util;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * This class encapsulates method for checking correctness of the student's login and password.
 * @author Igor Pavlyushchik
 *         Created on March 31, 2015.
 */
public class LoginLogic {
    /**
     * This method checks if the entered login and password are valid login and password of the student.
     * In positive scenario the method returns {@code Map<Login, Student>} map and in the negative -
     * null.
     * @param enterLogin  login entered by the student
     * @param enterPass    password entered by the student
     * @return  {@code Map<Login, Student>} which consists of the {@code Login} and {@code Student} entries
     * which correspond to the logged in student
     * @throws TechnicalException
     */
    public Map<Login, Student> checkLogin (String enterLogin, String enterPass) throws TechnicalException {
        Map<Login, Student> loginStudentMap;
        String password;
        try (StudentDAO studentDAO = new StudentDAO()){
            loginStudentMap = studentDAO.findStudent(enterLogin);
        } catch (DAOException e) {
            throw new TechnicalException("Unable to get login - student map." + e);
        }
        Set<Map.Entry<Login, Student>> entries = loginStudentMap.entrySet();
        Iterator<Map.Entry<Login, Student>> iterator = entries.iterator();
        if(iterator.hasNext()) {
            Map.Entry<Login, Student> loginStudentEntry = iterator.next();
            password = String.valueOf(loginStudentEntry.getKey().getPassword());
            enterPass = new MD5Util().md5Custom(enterPass);
            if(!password.equals(enterPass)) {
                loginStudentMap = null;
            }
        } else {
            loginStudentMap = null;
        }
        return loginStudentMap;
    }
}
