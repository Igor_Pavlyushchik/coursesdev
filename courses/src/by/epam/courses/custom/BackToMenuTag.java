package by.epam.courses.custom;


import by.epam.courses.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * This class creates custom tag functionality, encapsulating back_to_menu form which submits back_to_menu
 * command.
 * @author Igor Pavlyushchik
 *         Created on May 05, 2015.
 */
public class BackToMenuTag extends TagSupport {
    private static final long serialVersionUID = 8825772262245447498L;
    private HttpServletRequest request;
    public void setRequest(HttpServletRequest request) {
	this.request = request;
    }

    @Override
    public int doStartTag() throws JspException {
	String backToMenu = "<form name='back_to_menu' method='POST' action='controller'>" +
	"<input type='hidden' name='command' value='back_to_menu'/>" +
	"<input type='submit' value='" + new MessageManager(request).getProperty("student.back_to_menu") + "'/>" +
	"</form>";
	try {
	    JspWriter out = pageContext.getOut();
	    out.write(backToMenu);
	} catch (IOException e) {
	    throw new JspException(e.getMessage());
	}
	return SKIP_BODY;
    }
    @Override
    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }
}
