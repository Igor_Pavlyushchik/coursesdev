package by.epam.courses.custom;


import by.epam.courses.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * This class encapsulates custom {@literal "footer"} tag functionality. This tag is used to draw the footer of the
 * page with one tag.
 * @author Igor Pavlyushchik
 *         Created on May 05, 2015.
 */
public class FooterTag extends TagSupport {
    private static final long serialVersionUID = 8825772262245447498L;
    private HttpServletRequest request;
    public void setRequest(HttpServletRequest request) {
	this.request = request;
    }

    @Override
    public int doStartTag() throws JspException {
	String footer = "<div id=\"footer\"> <hr>" +
	    " <p style=\"text-align: center\">" + new MessageManager(request).getProperty("message.signature") +
	    "</p>  <hr></div>";
	try {
	    JspWriter out = pageContext.getOut();
	    out.write(footer);
	} catch (IOException e) {
	    throw new JspException(e.getMessage());
	}
	return SKIP_BODY;
    }
    @Override
    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }
}
