package by.epam.courses.comand;

import by.epam.courses.dao.LoginDAO;
import by.epam.courses.dao.StudentDAO;
import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.resource.ConfigurationManager;
import by.epam.courses.resource.MessageManager;
import by.epam.courses.subject.Login;
import by.epam.courses.subject.Student;
import by.epam.courses.util.MD5Util;

import javax.servlet.http.HttpServletRequest;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on April 07, 2015.
 */
public class StudentEditActionCommand implements ActionCommand {
    private static final String PHONE_PATTERN = "[1-9][\\d]{2,11}";
    private static final String PASSWORD_PATTERN = "[\\w]{3,8}";
    private static final String REQUEST_PARAM_STUDENT_ID = "student_id";
    private static final String REQUEST_PARAM_FIRST_NAME = "first_name";
    private static final String REQUEST_PARAM_SECOND_NAME = "second_name";
    private static final String REQUEST_PARAM_PHONE = "phone";
    private static final String REQUEST_PARAM_LOGIN = "login";
    private static final String REQUEST_PARAM_PASSWORD = "password";
    private static final String REQUEST_PARAM_PASSWORD2 = "password2";

    /**
     * This method is used to validate and update the Student entry with the possibility to change phone and password.
     * As a result the String value of the path to the student menu jsp is returned and the relevant session
     * attribute describing the outcome of the operation is set.
     * @param request {@code HttpServletRequest} of the user
     * @return  {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	String page;
	Student student = new Student();
	Login login = new Login();
	MessageManager messageManager = new MessageManager(request);
	int rowsNumber;
	int rowsNumberLogin;
	try (StudentDAO studentDAO = new StudentDAO();
	    LoginDAO loginDAO = new LoginDAO()){
	    boolean errorFlag = false;
	    student.setStudentId(Integer.parseInt(request.getParameter(REQUEST_PARAM_STUDENT_ID)));
	    student.setFirstName(request.getParameter(REQUEST_PARAM_FIRST_NAME));
	    student.setSecondName(request.getParameter(REQUEST_PARAM_SECOND_NAME));

	    String phone = request.getParameter(REQUEST_PARAM_PHONE);
	    if(phone.matches(PHONE_PATTERN)) {
		student.setPhone(Integer.parseInt(phone));
	    }  else {
		errorFlag = true;
		request.setAttribute("error_phone_pattern", messageManager.getProperty("error.phone.pattern"));
	    }
	    student.setLogin(request.getParameter(REQUEST_PARAM_LOGIN));
	    login.setLogin(request.getParameter(REQUEST_PARAM_LOGIN));

	    String password;
	    password = request.getParameter(REQUEST_PARAM_PASSWORD);
	    if(!password.matches(PASSWORD_PATTERN)) {
		errorFlag = true;
		request.setAttribute("error_password_pattern", messageManager.getProperty("error.password.pattern"));
	    }
	    if(password.equals(request.getParameter(REQUEST_PARAM_PASSWORD2))) {
		login.setPassword(new MD5Util().md5Custom(password));
	    } else {
		errorFlag = true;
		request.setAttribute("error_password_message", messageManager.getProperty("error.password.message"));
	    }
	    if(errorFlag) return ConfigurationManager.getProperty("path.page.student_edit_error");

	    rowsNumber = studentDAO.update(student);
	    rowsNumberLogin = loginDAO.update(login);
	} catch (DAOException e) {
	    throw new TechnicalException("Unable to update student and login in DB " + e);
	}

	if(rowsNumber == 1 && rowsNumberLogin == 1) {
	   request.setAttribute("update_result", messageManager.getProperty("student.menu.updated"));
	}  else {
	    request.setAttribute("update_result_problem", messageManager.getProperty("student.menu.not_updated"));
	}
	page = ConfigurationManager.getProperty("path.page.student_menu");
	return page;
    }
}
