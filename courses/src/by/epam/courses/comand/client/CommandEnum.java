package by.epam.courses.comand.client;

import by.epam.courses.comand.ActionCommand;
import by.epam.courses.comand.CancelCommand;
import by.epam.courses.comand.CoursesCommand;
import by.epam.courses.comand.DeleteStudentCommand;
import by.epam.courses.comand.FeedbackEditActionCommand;
import by.epam.courses.comand.FeedbackEditCommand;
import by.epam.courses.comand.LanguageCommand;
import by.epam.courses.comand.LanguageLoginCommand;
import by.epam.courses.comand.LoginCommand;
import by.epam.courses.comand.LogoutCommand;
import by.epam.courses.comand.ManageStudentsCommand;
import by.epam.courses.comand.RegisterCommand;
import by.epam.courses.comand.SignUpCommand;
import by.epam.courses.comand.StudentEditActionCommand;
import by.epam.courses.comand.StudentEditCommand;
import by.epam.courses.comand.StudentRegisterActionCommand;

/**
 * This enum is used to help generation of the relevant {@code ActionCommand} implementation instance,
 *  which is generated for each command name.
 * @author Igor Pavlyushchik
 *         Created on March 27, 2015.
 */
public enum CommandEnum {
    LOGIN {
	{
	    this.command = new LoginCommand();
	}
    },
    REGISTER {
	{
	    this.command = new RegisterCommand();
	}
    },
    LOGOUT {
	{
	    this.command = new LogoutCommand();
	}
    },
    STUDENT_EDIT {
	{
	    this.command = new StudentEditCommand();
	}
    },
    STUDENT_EDIT_ACTION {
	{
	    this.command = new StudentEditActionCommand();
	}
    },
    LANGUAGE {
	{
	    this.command = new LanguageCommand();
	}
    },
    LANGUAGE_LOGIN {
	{
	    this.command = new LanguageLoginCommand();
	}
    },
    COURSES {
	{
	    this.command = new CoursesCommand();
	}
    },
    SIGN_UP {
	{
	    this.command = new SignUpCommand();
	}
    },
    CANCEL {
	{
	    this.command = new CancelCommand();
	}
    },
    MANAGE_STUDENTS {
	{
	    this.command = new ManageStudentsCommand();
	}
    },
    EDIT_FEEDBACK {
	{
	    this.command = new FeedbackEditCommand();
	}
    },
    FEEDBACK_EDIT_ACTION {
	{
	    this.command = new FeedbackEditActionCommand();
	}
    },
    STUDENT_REGISTER_ACTION {
	{
	    this.command = new StudentRegisterActionCommand();
	}
    },
    DELETE_STUDENT {
	{
	    this.command = new DeleteStudentCommand();
	}
    },
    ;
    ActionCommand command;

    /**
     * This method returns instance of the proper command.
     * @return  proper instance of the {@code ActionCommand} interface.
     */
    public ActionCommand getCurrentCommand() {
	return command;
    }
}
