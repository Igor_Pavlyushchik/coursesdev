package by.epam.courses.comand;

import by.epam.courses.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on April 01, 2015.
 */
public class EmptyCommand implements ActionCommand {

    /**
     * This method returns {@code String} name of the menu professor or student jsp respectively, depending on
     * the value of the {@literal "role"} attribute. If the session is null or role has any other than
     * student or professor value the String name of the login jsp is returned.
     * @param request {@code HttpServletRequest} of the user
     * @return {@code String} value of the path to the jsp
     */
    @Override
    public String execute(HttpServletRequest request) {
	/*
	In case of mistake or direct addressing the controller
	redirecting to the login page or menu.
	 */
	String page = null;
	HttpSession session = request.getSession(false);
	if(session != null) {
	    if(session.getAttribute("role").equals("student")) {
		page = ConfigurationManager.getProperty("path.page.student_menu");
	    } else if(session.getAttribute("role").equals("professor")) {
		page = ConfigurationManager.getProperty("path.page.professor_menu");
	    }
	}
	if(page == null) {
	    request.getSession().invalidate();
	    page = ConfigurationManager.getProperty("path.page.login");
	}
	return page;
    }
}
