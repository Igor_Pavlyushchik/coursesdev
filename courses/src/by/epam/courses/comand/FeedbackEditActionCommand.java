package by.epam.courses.comand;

import by.epam.courses.dao.FeedbackDAO;
import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.logic.ManageStudentsLogic;
import by.epam.courses.resource.ConfigurationManager;
import by.epam.courses.resource.MessageManager;
import by.epam.courses.subject.Feedback;

import javax.servlet.http.HttpServletRequest;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on May 03, 2015.
 */
public class FeedbackEditActionCommand implements ActionCommand {
    private static final String FEEDBACK_PATTERN = ".{0,255}";
    private static final String REQUEST_PARAM_FEEDBACK = "feedback";
    private static final String REQUEST_PARAM_FEEDBACK_ID = "feedback_id";
    private static final String REQUEST_PARAM_STATUS = "status";
    private static final String REQUEST_PARAM_GRADE = "grade";

    /**
     * This method is used to update feedback of the current logged in professor for the given student and course.
     * {@code errorFlag} variable is set to true, if the feedback entered by the professor does not satisfy the
     * {@code FEEDBACK_PATTERN}. In this case {@literal "error_feedback_pattern} is assigned the relevant value.
     * and String name of the feedback_edit jsp is returned.
     * Otherwise, when {@code errorFlag} is false, feedback is updated and
     * {@link ManageStudentsLogic#manageStudents(HttpServletRequest)} method is called to return jsp String name
     * with the feedback table for the students of the given professor.
     * @param request {@code HttpServletRequest} of the user
     * @return {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	MessageManager messageManager = new MessageManager(request);
	try(FeedbackDAO feedbackDAO = new FeedbackDAO()) {
	    boolean errorFlag = false;
	    Feedback entity = new Feedback();
	    String feedback = request.getParameter(REQUEST_PARAM_FEEDBACK);
	    if(feedback.matches(FEEDBACK_PATTERN)){
		 entity.setFeedback(feedback);
	    }  else {
		errorFlag = true;
		request.setAttribute("error_feedback_pattern", messageManager.getProperty("error.feedback.pattern"));
	    }
	    entity.setFeedbackId(Integer.parseInt(request.getParameter(REQUEST_PARAM_FEEDBACK_ID)));
	    entity.setStatus(request.getParameter(REQUEST_PARAM_STATUS));
	    entity.setGrade(Integer.parseInt(request.getParameter(REQUEST_PARAM_GRADE)));
	    if(errorFlag) {
		return ConfigurationManager.getProperty("path.page.feedback_edit");
	    }
	    feedbackDAO.update(entity);

	}  catch (DAOException e) {
	   throw new TechnicalException("Unable to update feedback. " + e);
	}
	return new ManageStudentsLogic().manageStudents(request);
    }
}
