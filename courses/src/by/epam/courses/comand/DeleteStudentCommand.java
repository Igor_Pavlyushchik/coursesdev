package by.epam.courses.comand;

import by.epam.courses.dao.LoginDAO;
import by.epam.courses.dao.StudentDAO;
import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.logic.ManageStudentsLogic;
import by.epam.courses.subject.Login;
import by.epam.courses.subject.Student;

import javax.servlet.http.HttpServletRequest;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on May 08, 2015.
 */
public class DeleteStudentCommand implements ActionCommand {
    /**
     * This method deletes student and login entries of student from the Data Base by student login, and than executes
     * {@link ManageStudentsLogic#manageStudents(HttpServletRequest)}, which returns jsp String name, and adds the
     * map of students and feedback for the courses of the professor attached to the request.
     * @param request {@code HttpServletRequest} of the user
     * @return {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	try(StudentDAO studentDAO = new StudentDAO(); LoginDAO loginDAO = new LoginDAO()) {
	    Student student = new Student();
	    Login login = new Login();
	    student.setLogin(request.getParameter("login"));
	    login.setLogin(request.getParameter("login"));
	    studentDAO.delete(student);
	    loginDAO.delete(login);
	} catch (DAOException e) {
	    throw new TechnicalException("Unable to delete student. " + e);
	}
	return new ManageStudentsLogic().manageStudents(request);
    }
}
