package by.epam.courses.comand.factory;

import by.epam.courses.comand.ActionCommand;
import by.epam.courses.comand.EmptyCommand;
import by.epam.courses.comand.client.CommandEnum;
import by.epam.courses.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * This class implements Factory pattern to return instance of the proper implementation of the {@code ActionCommand}
 * interface.
 * @author Igor Pavlyushchik
 *         Created on March 25, 2015.
 */
public class ActionFactory {
    private static final String BACK_TO_MENU_BUTTON = "back_to_menu";

    /**
     * This method extracts command name from the request, than if it is null, empty or {@literal "back_to_menu"}
     * {@code EmptyCommand} instance is returned. Otherwise with the help of {@code CurrentEnum} relevant
     * instance of {@code ActionCommand} implementation is returned. In the case of {@code IllegalArgumentException}
     * {@literal "wrongAction"} attribute is added to the request and {@code EmptyCommand} is returned.
     * @param request  received {@code HttpServletRequest}
     * @return  {@code ActionCommand} reference to the relevant implementation instance
     */
    public ActionCommand defineCommand (HttpServletRequest request) {
	ActionCommand current;
	// extracting command name from the request
	String action = request.getParameter("command");
	if (action == null || action.isEmpty() || BACK_TO_MENU_BUTTON.equals(action)) {
	// if there is no command in the current request
	    return new EmptyCommand();
	}
	// receiving the object corresponding to the command
	try {
	    CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
	    current = currentEnum.getCurrentCommand();
	} catch (IllegalArgumentException e) {
	    request.setAttribute("wrongAction", new MessageManager(request).getProperty("message.wrong_action") +
		action);
	    return new EmptyCommand();
	}
	return current;
    }
}
