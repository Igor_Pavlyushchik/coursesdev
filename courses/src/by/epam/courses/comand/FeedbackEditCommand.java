package by.epam.courses.comand;

import by.epam.courses.exception.TechnicalException;
import by.epam.courses.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on May 03, 2015.
 */
public class FeedbackEditCommand implements ActionCommand {

    /**
     * This method simply returns String representation of the jsp for feedback editing.
     * @param request {@code HttpServletRequest} of the user
     * @return  {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	return ConfigurationManager.getProperty("path.page.feedback_edit");
    }
}
