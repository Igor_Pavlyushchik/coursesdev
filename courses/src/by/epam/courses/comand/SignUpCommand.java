package by.epam.courses.comand;

import by.epam.courses.dao.FeedbackDAO;
import by.epam.courses.dao.StudentDAO;
import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.logic.ShowCoursesLogic;
import by.epam.courses.subject.Feedback;

import javax.servlet.http.HttpServletRequest;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on April 28, 2015.
 */
public class SignUpCommand implements ActionCommand {
    private static final String COURSE_ID_CONSTANT = "course_id";

    /**
     * This method is used to sign up the current student for the course and returns String value of the path
     * to the student courses jsp.
     * @param request {@code HttpServletRequest} of the user
     * @return {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	try (StudentDAO studentDAO = new StudentDAO(); FeedbackDAO feedbackDAO = new FeedbackDAO()) {
	    String login = (String)request.getSession().getAttribute("login");
	    int studentId = studentDAO.findStudentId(login);
	    int courseId = Integer.parseInt(request.getParameter(COURSE_ID_CONSTANT));
	    Feedback feedback = new Feedback();
	    feedback.setCourseId(courseId);
	    feedback.setStudentId(studentId);
	    feedbackDAO.create(feedback);
	}  catch (DAOException e) {
	    throw new TechnicalException("Unable to sign up student for the course. " + e);
	}
	return new ShowCoursesLogic().prepareCoursesPage(request);
    }
}
