package by.epam.courses.comand;

import by.epam.courses.exception.TechnicalException;

import javax.servlet.http.HttpServletRequest;

/**
 * This interface {@code ActionCommand}defines single method, which returns {@code String} reference to the jsp,
 * which will be executed as a response to user request.
 * @author Igor Pavlyushchik
 *         Created on March 27, 2015.
 */
public interface ActionCommand {
    /**
     * The implementation of this method returns {@code String} name of the jsp, which will be displayed to the
     * user as a response to the user request.
     * @param request  {@code HttpServletRequest} of the user
     * @return   {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    String execute(HttpServletRequest request) throws TechnicalException;
}
