package by.epam.courses.comand;

import by.epam.courses.dao.StudentDAO;
import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.resource.ConfigurationManager;
import by.epam.courses.subject.Login;
import by.epam.courses.subject.Student;

import javax.servlet.http.HttpServletRequest;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on April 07, 2015.
 */
public class StudentEditCommand implements ActionCommand {
    /**
     * This method returns value of the path to the student edit jsp with the current values to be edited.
     * @param request {@code HttpServletRequest} of the user
     * @return  {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	String page;
	Student student = null;
	Login login = null;
	Map<Login, Student> loginStudentMap;
	try (StudentDAO studentDAO = new StudentDAO()){
	    loginStudentMap = studentDAO.findStudent((String)request.getSession().getAttribute("login"));
	} catch (DAOException e) {
	    throw new TechnicalException("Unable to extract student and login from DB " + e);
	}
	Set<Map.Entry<Login, Student>> entries = loginStudentMap.entrySet();
	Iterator<Map.Entry<Login, Student>> iterator = entries.iterator();
	if(iterator.hasNext()) {
	    Map.Entry<Login, Student> loginStudentEntry = iterator.next();
	    student = loginStudentEntry.getValue();
	    login = loginStudentEntry.getKey();
	}
	request.setAttribute("student", student);
	request.setAttribute("login", login);
	page = ConfigurationManager.getProperty("path.page.student_edit");
	return page;
    }
}
