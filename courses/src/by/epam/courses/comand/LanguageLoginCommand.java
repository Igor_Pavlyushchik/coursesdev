package by.epam.courses.comand;

import by.epam.courses.exception.TechnicalException;
import by.epam.courses.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on May 07, 2015.
 */
public class LanguageLoginCommand implements ActionCommand {
    private final static String RUSSIAN_LOCALE = "ru_RU";
    private final static String ENGLISH_LOCALE = "en_US";
    private final static String PARAM_NAME_LOCALE = "locale";

    /**
     * This method is used to switch between two languages of the application from the login page before the user
     * has logged in.
     * @param request {@code HttpServletRequest} of the user
     * @return  {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	HttpSession session = request.getSession();
	String locale = (String)session.getAttribute(PARAM_NAME_LOCALE);
	if(ENGLISH_LOCALE.equals(locale) || locale == null) {
	    session.setAttribute("locale", RUSSIAN_LOCALE);
	}  else {
	    session.setAttribute("locale", ENGLISH_LOCALE);
	}
	return ConfigurationManager.getProperty("path.page.login");
    }

}

