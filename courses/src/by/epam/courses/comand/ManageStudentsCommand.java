package by.epam.courses.comand;

import by.epam.courses.exception.TechnicalException;
import by.epam.courses.logic.ManageStudentsLogic;

import javax.servlet.http.HttpServletRequest;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on April 29, 2015.
 */
public class ManageStudentsCommand implements ActionCommand {
    /**
     * This method calls {@link ManageStudentsLogic#manageStudents(HttpServletRequest)} method to return the
     * value of the path to the jsp with the feedback table for all students and courses of the current professor.
     * @param request {@code HttpServletRequest} of the user
     * @return {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	return new ManageStudentsLogic().manageStudents(request);
    }
}
