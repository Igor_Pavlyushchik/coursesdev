package by.epam.courses.comand;

import by.epam.courses.dao.LoginDAO;
import by.epam.courses.dao.StudentDAO;
import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.resource.ConfigurationManager;
import by.epam.courses.resource.MessageManager;
import by.epam.courses.subject.Login;
import by.epam.courses.subject.Student;
import by.epam.courses.util.MD5Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on May 08, 2015.
 */
public class StudentRegisterActionCommand implements ActionCommand {
    private static final String PHONE_PATTERN = "[1-9][\\d]{2,11}";
    private static final String PASSWORD_PATTERN = "[\\w]{3,8}";
    private static final String FIRST_NAME_PATTERN = ".{1,45}";
    private static final String SECOND_NAME_PATTERN = ".{1,45}";
    private static final String LOGIN_PATTERN = "\\w+@\\w+\\.[a-z]{2,4}";
    private static final String REQUEST_PARAM_FIRST_NAME = "first_name";
    private static final String REQUEST_PARAM_SECOND_NAME = "second_name";
    private static final String REQUEST_PARAM_PHONE = "phone";
    private static final String REQUEST_PARAM_LOGIN = "login";
    private static final String REQUEST_PARAM_PASSWORD = "password";
    private static final String REQUEST_PARAM_PASSWORD2 = "password2";
    private static final String SESSION_ATTRIBUTE_STUDENT = "student";

    /**
     * This method is used to validate and set values of the {@code Student} and {@code Login} entries while
     * registering the student. In the case of the failed validation relevant request attributes are set, error flag
     * is set to true and the value of the path to the student register jsp is returned again. If everything is OK
     * the new student and login entries are created, the new student is logged in and the value of the path to
     * the student menu jsp is returned.
     * @param request {@code HttpServletRequest} of the user
     * @return  {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	String page;
	Student student = new Student();
	Login login = new Login();
	HttpSession session = request.getSession();
	MessageManager messageManager = new MessageManager(request);
	try (StudentDAO studentDAO = new StudentDAO();
	     LoginDAO loginDAO = new LoginDAO()){
	    boolean errorFlag = false;

	    String firstName = request.getParameter(REQUEST_PARAM_FIRST_NAME);
	    if(firstName.matches(FIRST_NAME_PATTERN)) {
		student.setFirstName(firstName);
	    } else {
		errorFlag = true;
		request.setAttribute("error_first_name_pattern", messageManager.getProperty("error.first_name.pattern"));
	    }

	    String secondName = request.getParameter(REQUEST_PARAM_SECOND_NAME);
	    if(secondName.matches(SECOND_NAME_PATTERN)) {
		student.setSecondName(secondName);
	    } else {
		errorFlag = true;
		request.setAttribute("error_second_name_pattern", messageManager.getProperty("error.second_name.pattern"));
	    }

	    String phone = request.getParameter(REQUEST_PARAM_PHONE);
	    if(phone.matches(PHONE_PATTERN)) {
		student.setPhone(Integer.parseInt(phone));
	    }  else {
		errorFlag = true;
		request.setAttribute("error_phone_pattern", messageManager.getProperty("error.phone.pattern"));
	    }

	    String password = request.getParameter(REQUEST_PARAM_PASSWORD);
	    if(!password.matches(PASSWORD_PATTERN)) {
		errorFlag = true;
		request.setAttribute("error_password_pattern", messageManager.getProperty("error.password.pattern"));
	    }
	    if(password.equals(request.getParameter(REQUEST_PARAM_PASSWORD2))) {
		login.setPassword(new MD5Util().md5Custom(password));
	    } else {
		errorFlag = true;
		request.setAttribute("error_password_message", messageManager.getProperty("error.password.message"));
	    }

	    String enteredLogin = request.getParameter(REQUEST_PARAM_LOGIN);
	    if(!enteredLogin.matches(LOGIN_PATTERN)) {
		errorFlag = true;
		request.setAttribute("error_login_pattern", messageManager.getProperty("error.login.pattern"));
	    }  else {
		List<Login> loginList = loginDAO.findAll();
		for(Login loginEntry : loginList) {
		    if(loginEntry.getLogin().equals(enteredLogin)) {
			errorFlag = true;
			request.setAttribute("error_login_double", messageManager.getProperty("error.login.double"));
			break;
		    }
		    student.setLogin(enteredLogin);
		    login.setLogin(enteredLogin);
		}
	    }

	    if(errorFlag) return ConfigurationManager.getProperty("path.page.register");

	    loginDAO.create(login);
	    studentDAO.create(student);
	    session.setAttribute("first_name", student.getFirstName());
	    session.setAttribute("second_name", student.getSecondName());
	    session.setAttribute("login", student.getLogin());
	    session.setAttribute("role", SESSION_ATTRIBUTE_STUDENT);
	} catch (DAOException e) {
	    throw new TechnicalException("Unable to update student and login in DB " + e);
	}

	page = ConfigurationManager.getProperty("path.page.student_menu");
	return page;
    }
}
