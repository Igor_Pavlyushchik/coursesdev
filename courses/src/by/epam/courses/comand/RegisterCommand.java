package by.epam.courses.comand;

import by.epam.courses.exception.TechnicalException;
import by.epam.courses.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on May 08, 2015.
 */
public class RegisterCommand implements ActionCommand {
    private static final String LANGUAGE_EN_COUNTRY_US = "en_US";

    /**
     * This method sets locale attribute to the session for the default case and returns value of the path to the
     * register jsp, which is used to register a new student.
     * @param request {@code HttpServletRequest} of the user
     * @return {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
        HttpSession session = request.getSession();
        String locale = (String)session.getAttribute("locale");
        if(locale == null)  session.setAttribute("locale", LANGUAGE_EN_COUNTRY_US);
	return ConfigurationManager.getProperty("path.page.register");
    }
}
