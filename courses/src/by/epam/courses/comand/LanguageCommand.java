package by.epam.courses.comand;

import by.epam.courses.exception.TechnicalException;
import by.epam.courses.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on April 08, 2015.
 */
public class LanguageCommand implements ActionCommand {
    private final static String RUSSIAN_LOCALE = "ru_RU";
    private final static String ENGLISH_LOCALE = "en_US";
    private final static String PARAM_NAME_LOCALE = "locale";
    private final static String PARAM_NAME_ROLE = "role";
    private final static String STUDENT_ROLE = "student";

    /**
     * This method is used to switch between two languages of the application by the one push of the switch
     * button from the student or professor menu. The method changes locale session attribute value, and uses
     * role attribute value to return corresponding String value of the student or professor menu jsp.
     * @param request {@code HttpServletRequest} of the user
     * @return  {@code String} name of the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	HttpSession session = request.getSession();
	String locale = (String)session.getAttribute(PARAM_NAME_LOCALE);
	String role = (String)session.getAttribute(PARAM_NAME_ROLE);
	if(ENGLISH_LOCALE.equals(locale)) {
	    session.setAttribute("locale", RUSSIAN_LOCALE);
	}  else {
	    session.setAttribute("locale", ENGLISH_LOCALE);
	}
	if(STUDENT_ROLE.equals(role)) {
	    return ConfigurationManager.getProperty("path.page.student_menu");
	} else {
	    return ConfigurationManager.getProperty("path.page.professor_menu");
	}

    }
}
