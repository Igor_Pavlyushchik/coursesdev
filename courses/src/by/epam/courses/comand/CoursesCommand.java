package by.epam.courses.comand;

import by.epam.courses.exception.TechnicalException;
import by.epam.courses.logic.ShowCoursesLogic;

import javax.servlet.http.HttpServletRequest;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on April 26, 2015.
 */
public class CoursesCommand implements ActionCommand {

    /**
     * {@link ShowCoursesLogic#prepareCoursesPage} method is called which assigns to the request list of
     * feedback for every course possible for the student and returns String reference to the jsp, displaying this
     * information.
     * @param request {@code HttpServletRequest} of the user
     * @return   {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	return new ShowCoursesLogic().prepareCoursesPage(request);
    }
}
