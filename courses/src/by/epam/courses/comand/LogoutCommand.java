package by.epam.courses.comand;

import by.epam.courses.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on April 01, 2015.
 */
public class LogoutCommand implements ActionCommand {

    /**
     * On receiving the proper command this method invalidates session and returns the String value of the path
     * to the index jsp.
     * @param request {@code HttpServletRequest} of the user
     * @return  {@code String} value of the path to the jsp
     */
    @Override
    public String execute(HttpServletRequest request) {
	String page = ConfigurationManager.getProperty("path.page.index");
	// invalidating session
	request.getSession().invalidate();
	return page;
    }
}
