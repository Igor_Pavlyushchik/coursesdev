package by.epam.courses.comand;

import by.epam.courses.dao.FeedbackDAO;
import by.epam.courses.exception.DAOException;
import by.epam.courses.exception.TechnicalException;
import by.epam.courses.logic.ShowCoursesLogic;

import javax.servlet.http.HttpServletRequest;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on April 28, 2015.
 */
public class CancelCommand implements ActionCommand {
    private static final String FEEDBACK_ID_CONSTANT = "feedback_id";

    /**
     * This method deletes feedback entry by id received from request
     * which is by the logic of the application is equivalent to the signing out of the student from the course.
     * Than {@link ShowCoursesLogic#prepareCoursesPage} method is called which assigns to the request list of
     * feedback for every course possible for the student and returns String reference to the jsp, displaying this
     * information.
     * @param request {@code HttpServletRequest} of the user
     * @return {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
        try (FeedbackDAO feedbackDAO = new FeedbackDAO()) {
            int feedbackId = Integer.parseInt(request.getParameter(FEEDBACK_ID_CONSTANT));
            feedbackDAO.delete(feedbackId);
        }  catch (DAOException e) {
            throw new TechnicalException("Unable to sign up student for the course. " + e);
        }
        ShowCoursesLogic showCoursesLogic = new ShowCoursesLogic();
        return showCoursesLogic.prepareCoursesPage(request);
    }
}
