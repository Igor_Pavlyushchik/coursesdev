package by.epam.courses.comand;

import by.epam.courses.exception.TechnicalException;
import by.epam.courses.logic.LoginLogic;
import by.epam.courses.logic.LoginProfessorLogic;
import by.epam.courses.resource.ConfigurationManager;
import by.epam.courses.resource.MessageManager;
import by.epam.courses.subject.Login;
import by.epam.courses.subject.Professor;
import by.epam.courses.subject.Student;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * {@inheritDoc}
 * @author Igor Pavlyushchik
 *         Created on March 27, 2015.
 */
public class LoginCommand implements ActionCommand {
    private final static String PARAM_NAME_LOGIN = "login";
    private final static String PARAM_NAME_PASSWORD = "password";
    private static final String LANGUAGE_EN_COUNTRY_US = "en_US";

    /**
     * This method is used to process login request of the both possible uses of the system. If entered login and
     * password are correspond to the existing student, the String value of the path to the student menu jsp is
     * returned and a number of session attribute is set,
     * otherwise, if login and password correspond to the valid professor entry the String value of the path
     * to the professor menu jsp is returned and a number of the session attributes is set.
     * If the user is not found between students and professors the {@literal "errorLoginPassMessage"} session
     * attribute is set and the path value to the login jsp is returned.
     * @param request {@code HttpServletRequest} of the user
     * @return  {@code String} value of the path to the jsp
     * @throws TechnicalException
     */
    @Override
    public String execute(HttpServletRequest request) throws TechnicalException {
	String page;
	// Extracting login and password from the request
	String login = request.getParameter(PARAM_NAME_LOGIN);
	String pass = request.getParameter(PARAM_NAME_PASSWORD);
	// Checking login and password
	LoginLogic loginLogic = new LoginLogic();
	LoginProfessorLogic loginProfessorLogic = new LoginProfessorLogic();
	Map<Login, Student> loginStudentMap;
	Map<Login, Professor> loginProfessorMap;
	try {
	    loginStudentMap = loginLogic.checkLogin(login, pass);
	} catch (TechnicalException e) {
	    throw new TechnicalException("Unable to find student and password. " + e);
	}
	if(loginStudentMap != null) {
	    Set<Map.Entry<Login, Student>> entries = loginStudentMap.entrySet();
	    Iterator<Map.Entry<Login, Student>> iterator = entries.iterator();
	    if(iterator.hasNext()) {
		Map.Entry<Login, Student> loginStudentEntry = iterator.next();
		Student student = loginStudentEntry.getValue();
		HttpSession session = request.getSession();
		session.setAttribute("first_name", student.getFirstName());
		session.setAttribute("second_name", student.getSecondName());
		session.setAttribute("login", student.getLogin());
		if(session.getAttribute("locale") == null) session.setAttribute("locale", LANGUAGE_EN_COUNTRY_US);
		session.setAttribute("role", loginStudentEntry.getKey().getRole());
	    }
	    //setting path to the response jsp(student_menu.jsp)
	    page = ConfigurationManager.getProperty("path.page.student_menu");
	} else {
	   try {
	       loginProfessorMap = loginProfessorLogic.checkLogin(login, pass);
	   } catch (TechnicalException e) {
	       throw new TechnicalException("Unable to find professor and password. " + e);
	   }
	    if(loginProfessorMap != null) {
		Set<Map.Entry<Login, Professor>> entries = loginProfessorMap.entrySet();
		Iterator<Map.Entry<Login, Professor>> iterator = entries.iterator();
		if(iterator.hasNext()) {
		    Map.Entry<Login, Professor> loginProfessorEntry = iterator.next();
		    Professor professor = loginProfessorEntry.getValue();
		    HttpSession session = request.getSession();
		    session.setAttribute("first_name", professor.getFirstName());
		    session.setAttribute("second_name", professor.getSecondName());
		    session.setAttribute("login", professor.getLogin());
		    if(session.getAttribute("locale") == null) session.setAttribute("locale", LANGUAGE_EN_COUNTRY_US);
		    session.setAttribute("role", loginProfessorEntry.getKey().getRole());
		}
		//setting path to the response jsp(professor_menu.jsp)
		page = ConfigurationManager.getProperty("path.page.professor_menu");
	    }  else {
		request.setAttribute("errorLoginPassMessage",
		    new MessageManager(request).getProperty("message.login_error"));
		page = ConfigurationManager.getProperty("path.page.login");
	    }
	}
	return page;
    }
}
