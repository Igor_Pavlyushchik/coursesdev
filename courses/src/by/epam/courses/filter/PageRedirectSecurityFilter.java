package by.epam.courses.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This filter is used to redirect all direct requests to the index.jsp page in order to avoid incorrect use of the
 * application facilities.
 * @author Igor Pavlyushchik
 *         Created on April 07, 2015.
 */
@WebFilter( urlPatterns = { "/jsp/*", "/controller*" },
    initParams = { @WebInitParam(name = "INDEX_PATH", value = "/index.jsp") })
public class PageRedirectSecurityFilter implements Filter {
    private String indexPath;

    public void init(FilterConfig fConfig) throws ServletException {
	indexPath = fConfig.getInitParameter("INDEX_PATH");
    }

    public void doFilter(ServletRequest request, ServletResponse response,
			 FilterChain chain) throws IOException, ServletException {
	HttpServletRequest httpRequest = (HttpServletRequest) request;
	HttpServletResponse httpResponse = (HttpServletResponse) response;
// redirecting to the set page
	httpResponse.sendRedirect(httpRequest.getContextPath() + indexPath);
	chain.doFilter(request, response);
    }

    public void destroy() {
    }
}