package by.epam.courses.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import java.io.IOException;

/**
 * This filter is used for the correct interpretation of the cyrillic symbols transmitted with the request.
 * @author Igor Pavlyushchik
 *         Created on May 05, 2015.
 */
@WebFilter(urlPatterns = {"/*"}, initParams = {@WebInitParam(name = "encoding", value = "UTF-8", description = "Encoding Param")})
public class EncodingFilter implements Filter {
    private String code;

    public void init(FilterConfig fConfig) throws ServletException {
	code = fConfig.getInitParameter("encoding");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	String codeRequest = request.getCharacterEncoding();
	// setting encoding from the filter parameters, if it is not set.
	if (code != null && !code.equalsIgnoreCase(codeRequest)) {
	    request.setCharacterEncoding(code);
	    response.setCharacterEncoding(code);
	}
	chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
	code = null;
    }
}