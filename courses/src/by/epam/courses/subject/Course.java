package by.epam.courses.subject;

/**
 * @author Igor Pavlyushchik
 *         Created on April 05, 2015.
 */
public class Course extends Entity {
    private static final long serialVersionUID = -248023459195511312L;

    private int courseId;
    private String title;
    private int professorId;

    public Course() {
    }

    public Course(int courseId, String title, int professorId) {
	this.courseId = courseId;
	this.title = title;
	this.professorId = professorId;
    }

    public int getCourseId() {
	return courseId;
    }

    public void setCourseId(int courseId) {
	this.courseId = courseId;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public int getProfessorId() {
	return professorId;
    }

    public void setProfessorId(int professorId) {
	this.professorId = professorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Course)) return false;

        Course course = (Course) o;

        if (courseId != course.courseId) return false;
        if (professorId != course.professorId) return false;
        return !(title != null ? !title.equals(course.title) : course.title != null);

    }

    @Override
    public int hashCode() {
        int result = courseId;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + professorId;
        return result;
    }

    @Override
    public String toString() {
        return "Course{" +
            "courseId=" + courseId +
            ", title='" + title + '\'' +
            ", professorId=" + professorId +
            "} " + super.toString();
    }
}
