package by.epam.courses.subject;

/**
 * @author Igor Pavlyushchik
 *         Created on April 05, 2015.
 */
public class Feedback extends Entity {
    private static final long serialVersionUID = 2994376372786958704L;

    private int feedbackId;
    private int courseId;
    private int studentId;
    private int grade;
    private String feedback;
    private String status;

    public Feedback() {
    }

    public Feedback(int feedbackId, int courseId, int studentId, int grade, String feedback, String status) {
	this.feedbackId = feedbackId;
	this.courseId = courseId;
	this.studentId = studentId;
	this.grade = grade;
	this.feedback = feedback;
	this.status = status;
    }

    public int getFeedbackId() {
	return feedbackId;
    }

    public void setFeedbackId(int feedbackId) {
	this.feedbackId = feedbackId;
    }

    public int getCourseId() {
	return courseId;
    }

    public void setCourseId(int courseId) {
	this.courseId = courseId;
    }

    public int getStudentId() {
	return studentId;
    }

    public void setStudentId(int studentId) {
	this.studentId = studentId;
    }

    public int getGrade() {
	return grade;
    }

    public void setGrade(int grade) {
	this.grade = grade;
    }

    public String getFeedback() {
	return feedback;
    }

    public void setFeedback(String feedback) {
	this.feedback = feedback;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Feedback)) return false;

        Feedback feedback1 = (Feedback) o;

        if (feedbackId != feedback1.feedbackId) return false;
        if (courseId != feedback1.courseId) return false;
        if (studentId != feedback1.studentId) return false;
        if (grade != feedback1.grade) return false;
        if (feedback != null ? !feedback.equals(feedback1.feedback) : feedback1.feedback != null) return false;
        return !(status != null ? !status.equals(feedback1.status) : feedback1.status != null);

    }

    @Override
    public int hashCode() {
        int result = feedbackId;
        result = 31 * result + courseId;
        result = 31 * result + studentId;
        result = 31 * result + grade;
        result = 31 * result + (feedback != null ? feedback.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Feedback{" +
            "feedbackId=" + feedbackId +
            ", courseId=" + courseId +
            ", studentId=" + studentId +
            ", grade=" + grade +
            ", feedback='" + feedback + '\'' +
            ", status='" + status + '\'' +
            "} " + super.toString();
    }
}
