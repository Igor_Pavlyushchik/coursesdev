package by.epam.courses.subject;

/**
 * @author Igor Pavlyushchik
 *         Created on April 05, 2015.
 */
public class Login extends Entity{
    private static final long serialVersionUID = -8445849441507361384L;

    private String login;
    private String password;
    private String role;

    public Login() {
    }

    public Login(String login, String password, String role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public String getLogin() {
	return login;
    }

    public void setLogin(String login) {
	this.login = login;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Login)) return false;

        Login login1 = (Login) o;

        if (login != null ? !login.equals(login1.login) : login1.login != null) return false;
        if (password != null ? !password.equals(login1.password) : login1.password != null) return false;
        return !(role != null ? !role.equals(login1.role) : login1.role != null);

    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }
}
