package by.epam.courses.subject;

/**
 * @author Igor Pavlyushchik
 *         Created on April 05, 2015.
 */
public class Student extends Entity {
    private static final long serialVersionUID = 1509674943395978046L;

    private int studentId;
    private String firstName;
    private String secondName;
    private int phone;
    private String login;

    public Student() {}

    public Student(int studentId, String firstName, String secondName, int phone, String login) {
	this.studentId = studentId;
	this.firstName = firstName;
	this.secondName = secondName;
	this.phone = phone;
	this.login = login;
    }

    public int getStudentId() {
	return studentId;
    }

    public void setStudentId(int studentId) {
	this.studentId = studentId;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getSecondName() {
	return secondName;
    }

    public void setSecondName(String secondName) {
	this.secondName = secondName;
    }

    public int getPhone() {
	return phone;
    }

    public void setPhone(int phone) {
	this.phone = phone;
    }

    public String getLogin() {
	return login;
    }

    public void setLogin(String login) {
	this.login = login;
    }

    @Override
    public String toString() {
        return "Student{" +
            "studentId=" + studentId +
            ", firstName='" + firstName + '\'' +
            ", secondName='" + secondName + '\'' +
            ", phone=" + phone +
            ", login='" + login + '\'' +
            "} ";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;

        Student student = (Student) o;

        if (studentId != student.studentId) return false;
        if (phone != student.phone) return false;
        if (firstName != null ? !firstName.equals(student.firstName) : student.firstName != null) return false;
        if (secondName != null ? !secondName.equals(student.secondName) : student.secondName != null) return false;
        return !(login != null ? !login.equals(student.login) : student.login != null);

    }

    @Override
    public int hashCode() {
        int result = studentId;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + phone;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        return result;
    }
}
