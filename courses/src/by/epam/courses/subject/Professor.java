package by.epam.courses.subject;

import java.util.Date;

/**
 * @author Igor Pavlyushchik
 *         Created on April 27, 2015.
 */
public class Professor extends Entity {
    private static final long serialVersionUID = 1402592264727074351L;

    private int professorId;
    private String firstName;
    private String secondName;
    private Date birthDate;
    private int phone;
    private String login;
    private int ssn;
    private String employmentType;
    private String site;
    private String scienceTitle;
    private String position;

    public Professor() {
    }

    public Professor(int professorId, String firstName, String secondName, Date birthDate, int phone, String login, int ssn, String employmentType, String site, String scienceTitle, String position) {
	this.professorId = professorId;
	this.firstName = firstName;
	this.secondName = secondName;
	this.birthDate = birthDate;
	this.phone = phone;
	this.login = login;
	this.ssn = ssn;
	this.employmentType = employmentType;
	this.site = site;
	this.scienceTitle = scienceTitle;
	this.position = position;
    }

    public int getProfessorId() {
	return professorId;
    }

    public void setProfessorId(int professorId) {
	this.professorId = professorId;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getSecondName() {
	return secondName;
    }

    public void setSecondName(String secondName) {
	this.secondName = secondName;
    }

    public Date getBirthDate() {
	return birthDate;
    }

    public void setBirthDate(Date birthDate) {
	this.birthDate = birthDate;
    }

    public int getPhone() {
	return phone;
    }

    public void setPhone(int phone) {
	this.phone = phone;
    }

    public String getLogin() {
	return login;
    }

    public void setLogin(String login) {
	this.login = login;
    }

    public int getSsn() {
	return ssn;
    }

    public void setSsn(int ssn) {
	this.ssn = ssn;
    }

    public String getEmploymentType() {
	return employmentType;
    }

    public void setEmploymentType(String employmentType) {
	this.employmentType = employmentType;
    }

    public String getSite() {
	return site;
    }

    public void setSite(String site) {
	this.site = site;
    }

    public String getScienceTitle() {
	return scienceTitle;
    }

    public void setScienceTitle(String scienceTitle) {
	this.scienceTitle = scienceTitle;
    }

    public String getPosition() {
	return position;
    }

    public void setPosition(String position) {
	this.position = position;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof Professor)) return false;

	Professor professor = (Professor) o;

	if (professorId != professor.professorId) return false;
	if (phone != professor.phone) return false;
	if (ssn != professor.ssn) return false;
	if (firstName != null ? !firstName.equals(professor.firstName) : professor.firstName != null) return false;
	if (secondName != null ? !secondName.equals(professor.secondName) : professor.secondName != null) return false;
	if (birthDate != null ? !birthDate.equals(professor.birthDate) : professor.birthDate != null) return false;
	if (login != null ? !login.equals(professor.login) : professor.login != null) return false;
	if (employmentType != null ? !employmentType.equals(professor.employmentType) : professor.employmentType != null)
	    return false;
	if (site != null ? !site.equals(professor.site) : professor.site != null) return false;
	if (scienceTitle != null ? !scienceTitle.equals(professor.scienceTitle) : professor.scienceTitle != null)
	    return false;
	return !(position != null ? !position.equals(professor.position) : professor.position != null);

    }

    @Override
    public int hashCode() {
	int result = professorId;
	result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
	result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
	result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
	result = 31 * result + phone;
	result = 31 * result + (login != null ? login.hashCode() : 0);
	result = 31 * result + ssn;
	result = 31 * result + (employmentType != null ? employmentType.hashCode() : 0);
	result = 31 * result + (site != null ? site.hashCode() : 0);
	result = 31 * result + (scienceTitle != null ? scienceTitle.hashCode() : 0);
	result = 31 * result + (position != null ? position.hashCode() : 0);
	return result;
    }

    @Override
    public String toString() {
	return "Professor{" +
	    "professorId=" + professorId +
	    ", firstName='" + firstName + '\'' +
	    ", secondName='" + secondName + '\'' +
	    ", birthDate=" + birthDate +
	    ", phone=" + phone +
	    ", login='" + login + '\'' +
	    ", ssn=" + ssn +
	    ", employmentType='" + employmentType + '\'' +
	    ", site='" + site + '\'' +
	    ", scienceTitle='" + scienceTitle + '\'' +
	    ", position='" + position + '\'' +
	    "} " + super.toString();
    }
}
