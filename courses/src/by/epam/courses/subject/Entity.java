package by.epam.courses.subject;

import java.io.Serializable;

/**
 * @author Igor Pavlyushchik
 *         Created on April 02, 2015.
 */
public abstract class Entity implements Serializable, Cloneable {
    private static final long serialVersionUID = -7763545719301466073L;

}
