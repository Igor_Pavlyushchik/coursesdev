package by.epam.courses.exception;

/**
 * This is a custom exception thrown by different methods in case of some mistakes, caused by different wrong values,
 * etc.
 * @author Igor Pavlyushchik
 *         Created on April 06, 2015.
 */
public class TechnicalException extends Exception {
    private static final long serialVersionUID = 2548485627947108959L;

    public TechnicalException() {
    }

    public TechnicalException(String message) {
	super(message);
    }

    public TechnicalException(String message, Throwable cause) {
	super(message, cause);
    }

    public TechnicalException(Throwable cause) {
	super(cause);
    }

    public TechnicalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
	super(message, cause, enableSuppression, writableStackTrace);
    }
}
