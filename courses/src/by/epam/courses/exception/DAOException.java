package by.epam.courses.exception;

/**
 * This is a custom exception thrown by DAO methods in case of mistakes during queries execution.
 * @author Igor Pavlyushchik
 *         Created on April 05, 2015.
 */
public class DAOException extends Exception {
    private static final long serialVersionUID = 452468462103845556L;

    public DAOException() {
    }

    public DAOException(String message) {
	super(message);
    }

    public DAOException(String message, Throwable cause) {
	super(message, cause);
    }

    public DAOException(Throwable cause) {
	super(cause);
    }

    public DAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
	super(message, cause, enableSuppression, writableStackTrace);
    }
}
