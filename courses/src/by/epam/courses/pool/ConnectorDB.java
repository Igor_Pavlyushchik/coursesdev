package by.epam.courses.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * This class encapsulates the single method responsible for establishing connections.
 * @author Igor Pavlyushchik
 *         Created on April 03, 2015.
 */
class ConnectorDB {
    /**
     * This method reads initialization information from the {@code ResourceBundle} and than establishes
     * connection to the database.
     * @return  {@code Connection} to the database.
     */
    public static Connection getConnection() {
	ResourceBundle resource = ResourceBundle.getBundle("resources.database");
	String url = resource.getString("db.url");
	String user = resource.getString("db.user");
	String pass = resource.getString("db.password");

	try {
	    return DriverManager.getConnection(url, user, pass);
	} catch (SQLException e) {
	    throw new ExceptionInInitializerError("Error trying to establish connection. It is some critical mistake " +
		"with the access to the database. Unfortunately the only solution for now is to close the page " +
		"and turn to the support service." + e);
	}
    }
}
