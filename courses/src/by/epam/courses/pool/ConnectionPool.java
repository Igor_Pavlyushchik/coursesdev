package by.epam.courses.pool;


import by.epam.courses.exception.TechnicalException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

   /**
    * This class encapsulates members which create and manipulate the pool of connections to the database.
    * @author Igor Pavlyushchik
    *         Created on April 05, 2015.
    */
public class ConnectionPool {
    static Logger logger = Logger.getLogger(ConnectionPool.class);
    private static final int POOL_SIZE = 10;
    private BlockingQueue<Connection> connectionQueue;
    private static ConnectionPool connectionPool = null;
    private static AtomicBoolean poolCreated = new AtomicBoolean(false);
    private static ReentrantLock lock = new ReentrantLock();
    private static AtomicBoolean allowedTaking = new AtomicBoolean(true);

       /**
	* This private constructor registers mysql.jdbc.Driver and than initializes thread-safe connectionQueue putting
	* there parameter-defined number of {@code Connection}s.
	* @param poolSize  number of the {@code Connection}s to be put to the pool.
	*/
    private ConnectionPool(int poolSize) {
	try {
	    DriverManager.registerDriver(new com.mysql.jdbc.Driver());
	} catch (SQLException e) {
	    throw new ExceptionInInitializerError("Error trying to register jdbc driver. It is critical" +
		" mistake with the access to the database. Unfortunately the only solution for now is to close" +
		" the page and turn to the support service." + e);
	}
	connectionQueue = new ArrayBlockingQueue<>(poolSize);
	for (int i = 0; i < poolSize; i++) {
	    connectionQueue.offer(ConnectorDB.getConnection());
	}
    }

       /**
	* The public static method providing access to the single instance  of the {@code ConnectionPool}
	* @return single instance of the {@code ConnectionPool}.
	*/
    public static ConnectionPool getConnectionPool () {
	if(!poolCreated.get()) {
	    try {
		lock.lock();
		if(!poolCreated.get()) {
		    connectionPool = new ConnectionPool(POOL_SIZE);
		    poolCreated.set(true);
		}
	    } finally {
		lock.unlock();
	    }
	}
	return connectionPool;
    }

       /**
	* This method provides Connection from the pool. The AtomicBoolean allowedTaking variable is set to true
	* until the pool started closing. The use of {@code BlockingQueue<Connection>} facilitates possibility
	* for the thread to wait for the busy connection to be released in case if they all are taken.
	* @return  {@code Connection} from the pool.
	* @throws TechnicalException
	*/
    public Connection getConnection() throws TechnicalException {
	try {
	    if(allowedTaking.get()) {
		return connectionQueue.take();
	    }  else {
		throw new TechnicalException("Unable to take connection from the pool because of closing application.");
	    }
	} catch (InterruptedException e) {
	    throw new TechnicalException("Unable to take connection from the pool." + e);
	}
    }

       /**
	* This method returns {@code Connection} to the pool
	* @param connection  {@code Connection to be returned}
	*/
    public void closeConnection(Connection connection) {
	connectionQueue.offer(connection);
    }

       /**
	* This method makes all connections to be closed. To highly increase probability of this it forbids
	* taking connections from the pool and than sleeps for 2 seconds.
	*/
    public void releasePool() {
	Connection connection;
	allowedTaking.set(false);
	try {
	    Thread.sleep(2000);
	} catch (InterruptedException e) {
	    logger.error("The thread is interrupted, while waiting before releasing the pool." + e);
	}
	while ((connection = connectionQueue.poll()) != null) {
	     try {
		 connection.close();
	     } catch (SQLException e) {
		 logger.error("Unable to close connection while trying to release pool." + e);
	     }
	 }
    }
}