package by.epam.courses.resource;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This class responsible for generating localized messages in the application.
 * @author Igor Pavlyushchik
 *         Created on April 01, 2015.
 */
public class MessageManager {
    private static final String LANGUAGE_RU = "ru";
    private static final String COUNTRY_RU = "RU";
    private static final String LANGUAGE_RU_COUNTRY_RU = "ru_RU";
    private ResourceBundle resourceBundle;
    // the class extracts information from the messages.properties file
    public MessageManager(HttpServletRequest request) {
        String locale = (String)request.getSession().getAttribute("locale");
        Locale current;
        if(locale != null) {
            if(locale.equals(LANGUAGE_RU_COUNTRY_RU)) {
                current = new Locale(LANGUAGE_RU, COUNTRY_RU);
            } else {
                current = Locale.US;
            }
        }
          else {
            current = Locale.US;
        }
        resourceBundle = ResourceBundle.getBundle("resources.messages", current);
    }
    public String getProperty (String key) {
	return resourceBundle.getString(key);
    }
}
