package by.epam.courses.resource;

import java.util.ResourceBundle;

/**
 * This class encapsulates method responsible for returning {@code String} value of the path to the jsp, by the key.
 * @author Igor Pavlyushchik
 *         Created on April 01, 2015.
 */
public class ConfigurationManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config");
    // the class extracts information from the config.properties file
    private ConfigurationManager() {}
    public static String getProperty (String key) {
	return resourceBundle.getString(key);
    }
}
