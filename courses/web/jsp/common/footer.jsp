<%--
  @author Igor Pavlyushchik
  Created on April 23, 2015.
  Time: 8:54 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
    <title>Footer</title>
</head>
<body>
<div id="footer">
<fmt:setBundle basename="resources.messages" var="rb" />
<hr>
<p style="text-align: center"><fmt:message key="message.signature" bundle="${ rb }" /></p>
    <hr>
</div>
</body>
</html>
