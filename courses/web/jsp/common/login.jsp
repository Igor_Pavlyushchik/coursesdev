<%--
  Created by IntelliJ IDEA.
  User: Acer
  Date: 3/25/2015
  Time: 11:34 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="custom_tags" %>
<c:choose>
    <c:when test="${locale == null}">
        <fmt:setLocale value="en_US" scope="session" />
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="${locale}" scope="session" />
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="resources.messages" var="rb" />
<html><head><title><fmt:message key="message.login.login" bundle="${ rb }" /></title>

</head>
<body>
<c:import url="/jsp/common/header.jsp" charEncoding="utf-8" />
<div id="content">
<table style="width: 100%">
    <tr>
        <td style="width : 50%">
<form name="loginForm" method="POST" action="controller">
    <input type="hidden" name="command" value="login" />
    <fmt:message key="message.login.login2" bundle="${ rb }" /> <br>
    <input type="text" name="login" value=""/>
    <br/><fmt:message key="message.login.password" bundle="${ rb }" /> <br>
    <input type="password" name="password" value=""/>
    <br>
    <%--<fmt:message key="message.choose.language" bundle="${ rb }" /><br>
    <select name="locale">
         <option value="en_US" selected><fmt:message key="message.login.english" bundle="${ rb }" /></option>
         <option value="ru_RU"><fmt:message key="message.login.russian" bundle="${ rb }" /></option>
    </select><br>--%>
    ${errorLoginPassMessage}
    ${wrongAction}
    ${nullPage}
    <br>
    <input type="submit" value="<fmt:message key="message.login.log_in" bundle="${ rb }" />" />
</form>
<form name = "register" method = "POST" action="controller">
    <input type="hidden" name="command" value="register" />
    <br>
    <input type="submit" value="<fmt:message key="message.login.register" bundle="${ rb }" />" />
</form>
        </td>
        <td style="width : 50%; text-align: left; vertical-align: top">
            <p><fmt:message key="student.menu.language" bundle="${ rb }" /></p>
            <form name="language" method="POST" action="controller">
                <input type="hidden" name="command" value="language_login" />
                <input type="submit" value="<fmt:message key="student.button.switch" bundle="${ rb }" />"/>
            </form>
        </td>
    </tr>
</table>
</div>
<ctg:footer request="${pageContext.request}"/>
</body></html>

