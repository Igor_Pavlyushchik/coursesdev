<%--
  @author Igor Pavlyushchik
  Created on April 23, 2015.
  Time: 8:54 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Header</title>
    <style>
        <%@include file='/css/style.css' %>
    </style>
</head>
<body>
<fmt:setBundle basename="resources.messages" var="rb" />
<div id="header1">
<hr>
<h2 style="text-align: center"><fmt:message key="message.title" bundle="${ rb }" /></h2>
<hr>
</div>
<div id="header2">
<c:choose>
<c:when test="${pageContext.session.getAttribute('login') != null}">
<table style="width: 100%">
    <tr>
        <td style="width: 50%">
<fmt:message key="student.menu.logged" bundle="${ rb }" /> ${pageContext.session.getAttribute('login')}.<br>
    <br> <fmt:message key="student.menu.welcome" bundle="${ rb }" />,&nbsp
            <fmt:message key="message.login.${pageContext.session.getAttribute('role')}" bundle="${ rb }" />,&nbsp
            ${pageContext.session.getAttribute("first_name")} ${pageContext.session.getAttribute('second_name')}!&nbsp
            <%--<c:if test="${pageContext.session.getAttribute('role') eq 'professor'}">
            <fmt:message key="header.course.hello" bundle="${ rb }" /> "${pageContext.session.getAttribute('course')}".
            </c:if>--%>
        </td>
        <td style="width: 50%; text-align: right">
            <form name="logoutForm" method="POST" action="controller">
                <input type="hidden" name="command" value="logout" />
            <input type="submit" value="<fmt:message key="student.menu.logout" bundle="${ rb }" />"/>
            </form>
        </td>
    </tr>
</table>
</c:when>
    <c:otherwise>
        <br><br>
    </c:otherwise>
</c:choose>
</div>
<hr>
</body>
</html>
