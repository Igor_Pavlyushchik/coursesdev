<%--
  Created by IntelliJ IDEA.
  User: Acer
  Date: 3/25/2015
  Time: 11:36 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="custom_tags" %>
<fmt:setLocale value="${locale}" scope="session" />
<fmt:setBundle basename="resources.messages" var="rb" />
<html><head><title><fmt:message key="professor.list.students" bundle="${ rb }" /></title>
  <style>
    table.t1 {
      border-collapse: collapse;
    }
    th.h1,td.d1 {
      padding: 10px;
      border: 1px solid black;
    }
  </style>

</head>
<body>
<c:import url="/jsp/common/header.jsp" charEncoding="utf-8" />
<div id="content">
<h2><fmt:message key="professor.students.table" bundle="${ rb }" /></h2>
<table class="t1">
  <tr>
    <th class="h1" ><fmt:message key="professor.students.number" bundle="${ rb }" /></th>
    <th class="h1" ><fmt:message key="professor.students.second_name" bundle="${ rb }" /></th>
    <th class="h1" ><fmt:message key="professor.students.first_name" bundle="${ rb }" /></th>
    <th class="h1" ><fmt:message key="professor.students.login" bundle="${ rb }" /></th>
    <th class="h1" ><fmt:message key="professor.students.course" bundle="${ rb }" /></th>
    <th class="h1"><fmt:message key="professor.students.status" bundle="${ rb }" /></th>
    <th class="h1"><fmt:message key="professor.students.grade" bundle="${ rb }" /></th>
    <th class="h1"><fmt:message key="professor.students.feedback" bundle="${ rb }" /></th>
    <th class="h1" ><fmt:message key="professor.students.actions" bundle="${ rb }" /></th>
  </tr>
  <c:set var="count" value="1" scope="page"/>
  <c:forEach var = "course" items="${courseList}" varStatus="status">
    <c:forEach var = "student" items="${studentList}" varStatus="course_status">
      <c:set var="feedback" value="${map.get(student).get(course)}" scope="page"/>
      <tr>
        <td class="d1"><c:out value="${count}"/> </td>
        <c:set var="count" value="${count+1}" scope="page"/>
        <td class="d1"><c:out value="${student.secondName}"/> </td>
        <td class="d1"><c:out value="${student.firstName}"/> </td>
        <td class="d1"><c:out value="${student.login}"/> </td>
        <td class="d1"><c:out value="${course.title}"/> </td>
        <td class="d1"><c:out value="${feedback.status}"/> </td>
        <td class="d1"><c:if test="${feedback.grade > 0}"> <c:out value="${feedback.grade}"/> </c:if></td>
        <td class="d1"><c:out value="${feedback.feedback}"/> </td>
        <td class="d1">
        <table><tr>
          <td>
            <c:if test="${!(feedback.status eq null)}">
            <form name="edit_feedback" method="post" action="controller">
              <input type="hidden" name="feedback_id" value="${feedback.feedbackId}">
              <input type="hidden" name="second_name" value="${student.secondName}">
              <input type="hidden" name="first_name" value="${student.firstName}">
              <input type="hidden" name="login" value="${student.login}">
              <input type="hidden" name="course" value="${course.title}">
              <input type="hidden" name="status" value="${feedback.status}">
              <input type="hidden" name="grade" value="${feedback.grade}">
              <input type="hidden" name="feedback" value="${feedback.feedback}">
              <input type="hidden" name="gradeArray" value="${gradeArray}">
              <input type="hidden" name="command" value="edit_feedback">
              <input type="submit" value="<fmt:message key="professor.students.edit_feedback" bundle="${ rb }"/>"/>
            </form>
            </c:if>
          </td>

          <td>
            <c:if test="${studentBooleanMap.get(student)}">
            <form name="delete_student" method="post" action="controller">
              <input type="hidden" name="command" value="delete_student">
              <input type="hidden" name="login" value="${student.login}">
              <input type="submit" value="<fmt:message key="professor.students.delete_student" bundle="${ rb }"/>"/>
            </form>
            </c:if>
          </td>
        </tr>
          </table>
        </td>
      </tr>
      <c:if test="${course_status.count == studentList.size()}">
        <tr><td colspan="9"></td> </tr>
      </c:if>
    </c:forEach>
  </c:forEach>
  <%--<c:forEach var="elem" items="${student_feedback_list}" varStatus="status">
    <tr>
      <td class="d1"><c:out value="${status.count}"/> </td>
      <td class="d1"><c:out value="${elem.key.secondName}"/> </td>
      <td class="d1"><c:out value="${elem.key.firstName}"/> </td>
      <td class="d1"><c:out value="${elem.key.login}"/> </td>
      <td class="d1"><c:out value="${courseMap.get(elem.value.courseId)}"/> </td>
      <td class="d1"><c:out value="${elem.value.status}"/> </td>
      <td class="d1"><c:if test="${elem.value.grade > 0}"> <c:out value="${elem.value.grade}"/> </c:if></td>
      <td class="d1"><c:out value="${elem.value.feedback}"/> </td>
      <td class="d1">
        <c:if test="${!(elem.value.status eq null)}">
          <form name="edit_feedback" method="post" action="controller">
            <input type="hidden" name="feedback_id" value="${elem.value.feedbackId}">
            <input type="hidden" name="second_name" value="${elem.key.secondName}">
            <input type="hidden" name="first_name" value="${elem.key.firstName}">
            <input type="hidden" name="login" value="${elem.key.login}">
            <input type="hidden" name="course" value="${courseMap.get(elem.value.courseId)}">
            <input type="hidden" name="status" value="${elem.value.status}">
            <input type="hidden" name="grade" value="${elem.value.grade}">
            <input type="hidden" name="feedback" value="${elem.value.feedback}">
            <input type="hidden" name="command" value="edit_feedback">
            <input type="submit" value="<fmt:message key="professor.students.edit_feedback" bundle="${ rb }"/>"/>
          </form>
        </c:if>
      </td>
    </tr>
  </c:forEach>--%>
</table>
<br>
<ctg:back_to_menu request="${pageContext.request}"/>
</div>
<ctg:footer request="${pageContext.request}"/>
</body></html>
