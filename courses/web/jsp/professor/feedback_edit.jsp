<%--
  @author Igor Pavlyushchik
  Created on April 07, 2015.
  Time: 5:51 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="custom_tags" %>

<fmt:setLocale value="${locale}" scope="session" />
<fmt:setBundle basename="resources.messages" var="rb" />

<html><head><title><fmt:message key="feedback.edit" bundle="${ rb }" /></title></head>
<body>
<c:import url="/jsp/common/header.jsp" charEncoding="utf-8" />
<div id="content">
<h2><fmt:message key="feedback.edit.change" bundle="${ rb }" /></h2>
<hr>
<form name="feedback_edit" method="POST" action="controller">
    <input type="hidden" name="command" value="feedback_edit_action" />
    <input type="hidden" name="feedback_id" value="${pageContext.request.getParameter("feedback_id")}"/>
    <br><fmt:message key="feedback.edit.first_name" bundle="${ rb }" />
        ${pageContext.request.getParameter("first_name")} <br>
    <input type="hidden" name="first_name" value="${pageContext.request.getParameter("first_name")}"/>
    <br><fmt:message key="feedback.edit.second_name" bundle="${ rb }" />
        ${pageContext.request.getParameter("second_name")} <br>
    <input type="hidden" name="second_name" value="${pageContext.request.getParameter("second_name")}"/>
    <br><fmt:message key="feedback.edit.login" bundle="${ rb }" />
        ${pageContext.request.getParameter("login")} <br>
    <input type="hidden" name="login" value="${pageContext.request.getParameter("login")}"/>
    <br><fmt:message key="feedback.edit.status" bundle="${ rb }" /> <br>
    <select name="status">
        <option value="not started" <c:if test="${pageContext.request.getParameter('status') eq 'not started'}">
            selected </c:if> >
            <fmt:message key="feedback.edit.not_started" bundle="${ rb }" /></option>
        <option value="in progress" <c:if test="${pageContext.request.getParameter('status') eq 'in progress'}">
            selected </c:if> >
                <fmt:message key="feedback.edit.in_progress" bundle="${ rb }" /></option>
        <option value="completed" <c:if test="${pageContext.request.getParameter('status') eq 'completed'}">
            selected </c:if> >
                <fmt:message key="feedback.edit.completed" bundle="${ rb }" /></option>
    </select><br>
    <br/><fmt:message key="feedback.edit.grade" bundle="${ rb }" /><br>
    <select name="grade">
       <c:forEach var="i" begin="0" end="10">
           <option value="${i}" <c:if test="${pageContext.request.getParameter('grade') eq i}">
               selected </c:if> >${i}</option>
       </c:forEach>
    </select><br>
    <h3 style="color:red" >${error_feedback_pattern}</h3>
    <br/><fmt:message key="feedback.edit.feedback" bundle="${ rb }" /><br>
    <input type="text" name="feedback" pattern=".{0,255}"
           title="<fmt:message key="feedback.feedback.title" bundle="${ rb }" />"
           value="${pageContext.request.getParameter('feedback')}" size="100" maxlength="255" /><br><br>
    <input type="submit" value="<fmt:message key="feedback.edit.save" bundle="${ rb }" />"/>
</form>
<ctg:back_to_menu request="${pageContext.request}"/>
</div>
<ctg:footer request="${pageContext.request}"/>
</body></html>
