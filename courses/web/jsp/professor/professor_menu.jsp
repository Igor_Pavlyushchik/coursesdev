<%--
  Created by IntelliJ IDEA.
  User: Acer
  Date: 3/25/2015
  Time: 11:36 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="custom_tags" %>
<fmt:setLocale value="${locale}" scope="session" />
<fmt:setBundle basename="resources.messages" var="rb" />
<html><head><title><fmt:message key="student.menu.menu" bundle="${ rb }" /></title>

</head>
<body>
<c:import url="/jsp/common/header.jsp" charEncoding="utf-8" />
<div id="content">
<h2 style="color:red"> ${wrongAction} </h2>
<h2><fmt:message key="professor.menu.choose" bundle="${ rb }" /></h2>
<hr>
<p><fmt:message key="professor.menu.manage" bundle="${ rb }" /></p>
<form name="edit" method="POST" action="controller">
  <input type="hidden" name="command" value="manage_students" />
  <input type="submit" value="<fmt:message key="professor.button.manage" bundle="${ rb }" />"/>
</form><hr/>
<p><fmt:message key="student.menu.language" bundle="${ rb }" /></p>
<form name="language" method="POST" action="controller">
  <input type="hidden" name="command" value="language" />
  <input type="submit" value="<fmt:message key="student.button.switch" bundle="${ rb }" />"/>
</form>
</div>
<ctg:footer request="${pageContext.request}"/>
</body></html>
