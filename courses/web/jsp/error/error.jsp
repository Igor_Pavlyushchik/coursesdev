<%--
  Created by IntelliJ IDEA.
  User: Acer
  Date: 3/25/2015
  Time: 11:37 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html><head><title>Error Page</title></head>
<body>
<fmt:setBundle basename="resources.messages" var="rb" />
<c:import url="/jsp/common/header.jsp" charEncoding="utf-8" />
<div id="content">
Request from ${pageContext.errorData.requestURI} is failed
<br/>
Servlet name or type: ${pageContext.errorData.servletName}
<br/>
Status code: ${pageContext.errorData.statusCode}
<br/>
Exception: ${pageContext.errorData.throwable}
</div>
<c:import url="/jsp/common/footer.jsp" charEncoding="utf-8" />
</body></html>
