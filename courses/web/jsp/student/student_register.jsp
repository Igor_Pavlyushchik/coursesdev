<%--
  @author Igor Pavlyushchik
  Created on April 07, 2015.
  Time: 5:51 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="custom_tags" %>

<fmt:setLocale value="${locale}" scope="session" />
<fmt:setBundle basename="resources.messages" var="rb" />

<html><head><title><fmt:message key="student.register" bundle="${ rb }" /></title></head>
<body>
<c:import url="/jsp/common/header.jsp" charEncoding="utf-8" />
<div id="content">
<h2><fmt:message key="student.register.input" bundle="${ rb }" /></h2>
<hr>
<form name="register_student" method="POST" action="controller">
    <input type="hidden" name="command" value="student_register_action" />
    <h3 style="color:red" >${error_login_pattern}</h3>
    <h3 style="color:red" >${error_login_double}</h3>
    <fmt:message key="student.register.login" bundle="${ rb }" /><br>
    <input type="email" name="login"
           title="<fmt:message key="student.register.login.title" bundle="${ rb }" />"
           value="${pageContext.request.getParameter("login")}" required/><br>
    <h3 style="color:red" >${error_password_message}</h3>
    <h3 style="color:red" >${error_password_pattern}</h3>
    <br><fmt:message key="student.register.password" bundle="${ rb }" /><br>
    <input type="password" name="password" pattern="\w{3,8}"
           title="<fmt:message key="student.password.title" bundle="${ rb }" />" required/><br>
    <br><fmt:message key="message.login.password2" bundle="${ rb }" /><br>
    <input type="password" name="password2" pattern="\w{3,8}"
           title="<fmt:message key="student.password.title" bundle="${ rb }" />" required/><br>
    <h3 style="color:red" >${error_first_name_pattern}</h3>
    <br/><fmt:message key="student.register.first_name" bundle="${ rb }" /><br>
    <input type="text" name="first_name" pattern=".{1,45}"
           title="<fmt:message key="student.register.first_name.title" bundle="${ rb }" />"
           value="${pageContext.request.getParameter("first_name")}" required/><br>
    <h3 style="color:red" >${error_second_name_pattern}</h3>
    <br><fmt:message key="student.register.second_name" bundle="${ rb }" /> <br>
    <input type="text" name="second_name" pattern=".{1,45}"
           title="<fmt:message key="student.register.second_name.title" bundle="${ rb }" />"
           value="${pageContext.request.getParameter("second_name")}" required/><br>
    <h3 style="color:red" >${error_phone_pattern}</h3>
    <br><fmt:message key="student.register.phone" bundle="${ rb }" /> <br>
    <input type="text" name="phone" pattern="[1-9]\d{2,11}"
           title="<fmt:message key="student.phone.title" bundle="${ rb }" />"
           value="${pageContext.request.getParameter("phone")}" required/>
    <br><br>
    <input type="submit" value="<fmt:message key="student.register.save" bundle="${ rb }" />"/>
</form>
</div>
<c:import url="/jsp/common/footer.jsp" charEncoding="utf-8" />
</body></html>
