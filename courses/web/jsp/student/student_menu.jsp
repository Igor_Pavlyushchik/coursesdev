<%--
  Created by IntelliJ IDEA.
  User: Acer
  Date: 3/25/2015
  Time: 11:36 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}" scope="session" />
<fmt:setBundle basename="resources.messages" var="rb" />
<html><head><title><fmt:message key="student.menu.menu" bundle="${ rb }" /></title></head>
<body>
<c:import url="/jsp/common/header.jsp" charEncoding="utf-8" />
<div id="content">
<h2 style="color:green"> ${update_result} </h2>
<h2 style="color:red"> ${update_result_problem} </h2>
<h2 style="color:red"> ${wrongAction} </h2>
<h2><fmt:message key="student.menu.choose" bundle="${ rb }" /></h2>
<hr>
<p><fmt:message key="student.menu.edit" bundle="${ rb }" /></p>
<form name="edit" method="POST" action="controller">
  <input type="hidden" name="command" value="student_edit" />
  <input type="submit" value="<fmt:message key="student.button.edit" bundle="${ rb }" />"/>
</form><hr/>
<p><fmt:message key="student.menu.view" bundle="${ rb }" /></p>
<form name="courses" method="POST" action="controller">
  <input type="hidden" name="command" value="courses" />
  <input type="submit" value="<fmt:message key="student.button.view" bundle="${ rb }" />"/>
</form><hr/>
<p><fmt:message key="student.menu.language" bundle="${ rb }" /></p>
<form name="language" method="POST" action="controller">
  <input type="hidden" name="command" value="language" />
  <input type="submit" value="<fmt:message key="student.button.switch" bundle="${ rb }" />"/>
</form>
</div>
<c:import url="/jsp/common/footer.jsp" charEncoding="utf-8" />
</body></html>
