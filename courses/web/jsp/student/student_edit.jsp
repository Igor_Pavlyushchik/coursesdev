<%--
  @author Igor Pavlyushchik
  Created on April 07, 2015.
  Time: 5:51 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="custom_tags" %>

<fmt:setLocale value="${locale}" scope="session" />
<fmt:setBundle basename="resources.messages" var="rb" />

<html><head><title><fmt:message key="student.edit.edit" bundle="${ rb }" /></title></head>
<body>
<c:import url="/jsp/common/header.jsp" charEncoding="utf-8" />
<div id="content">
<h2><fmt:message key="student.edit.change" bundle="${ rb }" /></h2>
<hr>
<form name="edit" method="POST" action="controller">
    <input type="hidden" name="command" value="student_edit_action" />
    <input type="hidden" name="student_id" value="${student.studentId}"/>
    <fmt:message key="student.edit.login" bundle="${ rb }" /><br>
    <input type="text" name="login" value="${student.login}" readonly /><br>
    <br><fmt:message key="message.login.password" bundle="${ rb }" /><br>
    <input type="password" name="password" pattern="\w{3,8}"
           title="<fmt:message key="student.password.title" bundle="${ rb }" />" required/><br>
    <br><fmt:message key="message.login.password2" bundle="${ rb }" /><br>
    <input type="password" name="password2" pattern="\w{3,8}"
           title="<fmt:message key="student.password.title" bundle="${ rb }" />" required/><br>
    <br/><fmt:message key="student.edit.first_name" bundle="${ rb }" /><br>
    <input type="text" name="first_name" value="${student.firstName}" readonly /><br>
    <br><fmt:message key="student.edit.second_name" bundle="${ rb }" /> <br>
    <input type="text" name="second_name" value="${student.secondName}" readonly /><br>
    <br><fmt:message key="student.edit.phone" bundle="${ rb }" /> <br>
    <input type="text" name="phone" pattern="[1-9]\d{2,11}"
           title="<fmt:message key="student.phone.title" bundle="${ rb }" />" value="${student.phone}" required/>
    <br><br>
    <input type="submit" value="<fmt:message key="student.edit.save" bundle="${ rb }" />"/>
</form>
<ctg:back_to_menu request="${pageContext.request}"/>
</div>
<c:import url="/jsp/common/footer.jsp" charEncoding="utf-8" />
</body></html>
