<%--
  @author Igor Pavlyushchik
  Created on April 26, 2015.
  Time: 6:29 PM
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="custom_tags" %>
<fmt:setLocale value="${locale}" scope="session" />
<fmt:setBundle basename="resources.messages" var="rb" />
<html><head><title><fmt:message key="student.courses.courses" bundle="${ rb }" /></title>
  <style>
    table.t1 {
      border-collapse: collapse;
    }
    th.h1,td.d1 {
      padding: 10px;
      border: 1px solid black;
    }
  </style>
</head>
<body>
<c:import url="/jsp/common/header.jsp" charEncoding="utf-8" />
<div id="content">
<h2><fmt:message key="student.courses.table" bundle="${ rb }" /></h2>
<table class="t1">
  <tr>
    <th class="h1"><fmt:message key="student.courses.number" bundle="${ rb }" /></th>
    <th class="h1"><fmt:message key="student.courses.title" bundle="${ rb }" /></th>
    <th class="h1"><fmt:message key="student.courses.status" bundle="${ rb }" /></th>
    <th class="h1"><fmt:message key="student.courses.feedback" bundle="${ rb }" /></th>
    <th class="h1"><fmt:message key="student.courses.grade" bundle="${ rb }" /></th>
    <th class="h1"><fmt:message key="student.courses.sign_up" bundle="${ rb }" /></th>
  </tr>
<c:forEach var="elem" items="${feedback_list}" varStatus="status">
  <tr>
    <td class="d1"><c:out value="${status.count}"/> </td>
    <td class="d1"><c:out value="${elem.key.title}"/> </td>
    <td class="d1"><c:out value="${elem.value.status}"/> </td>
    <td class="d1"><c:out value="${elem.value.feedback}"/> </td>
    <td class="d1"><c:out value="${elem.value.grade}"/> </td>
    <td class="d1">
      <c:if test="${elem.value.status == null}">
        <form name="signing" method="POST" action="controller">
          <input type="hidden" name="command" value="sign_up" />
          <input type="hidden" name="course_id" value="${elem.key.courseId}"/>
          <input type="submit" value="<fmt:message key="student.courses.sign_up" bundle="${ rb }" />"/>
        </form>
      </c:if>
      <c:if test="${elem.value.status eq 'not started'}">
        <form name="cancelling" method="POST" action="controller">
          <input type="hidden" name="command" value="cancel" />
          <input type="hidden" name="feedback_id" value="<c:out value="${elem.value.feedbackId}"/>"/>
          <input type="submit" value="<fmt:message key="student.courses.cancel" bundle="${ rb }" />"/>
        </form>
      </c:if>
    </td>
  </tr>
</c:forEach>
</table>
<br>
<ctg:back_to_menu request="${pageContext.request}"/>
</div>
<c:import url="/jsp/common/footer.jsp" charEncoding="utf-8" />
</body></html>
